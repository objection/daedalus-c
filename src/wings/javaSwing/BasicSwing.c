package wings.javaSwing;

#include java.javax.swing.JFrame;
#include java.javax.swing.JPanel;
#include java.awt.Color;
#include java.awt.Cursor;
#include java.awt.event.KeyEvent;
#include java.awt.event.KeyListener;
#include java.awt.event.MouseAdapter;
#include java.awt.event.MouseListener;
#include java.awt.event.MouseMotionListener;
#include java.awt.event.MouseEvent;
#include java.awt.Dimension;
#include java.lang.System;
#include java.awt.Graphics2D;
#include haxe.macro.Compiler;

class BasicSwing
extends JFrame
implements KeyListener
implements MouseListener
implements MouseMotionListener
{
    public var surface: Surface;
    public function new()
    {
        super( '' ); //'Java Daedalus Example' );//
		setTitle( Compiler.getDefine("windowTitle") );
        System.setProperty( "sun.java2d.opengl", "True" );
        var header = new SwingHeader();
        setSize( header.width, header.height );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setBackground( new Color( header.bgColor ) );
        surface = new Surface();
		getContentPane().setPreferredSize( new Dimension( header.width, header.height ) );
        getContentPane().add( surface );
        surface.addKeyListener( this );
        surface.setFocusable( true );
        surface.requestFocusInWindow();
        surface.addMouseListener( this );
        surface.addMouseMotionListener( this );
		pack();
        //surface.repaint();
        setVisible( true );
    }
    // Mouse and keyboard events ready to be used...
    public function mousePressed( e: MouseEvent ) {}
    public function mouseDragged( e: MouseEvent ) {}
    public function mouseExited( e: MouseEvent ) {}
    public function mouseMoved( e: MouseEvent ) {}
    public function mouseEntered( e: MouseEvent ) {}
    public function mouseClicked( e: MouseEvent ) {}
    public function mouseReleased( e: MouseEvent ){}
    public function keyTyped( e: KeyEvent ) {}
    public function keyReleased(e: KeyEvent ) {}
    public function keyPressed( e: KeyEvent ) {};
}
