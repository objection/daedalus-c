#include "entity-ai.h"

const int NUM_SEGMENTS = 6;

struct entity_ai entity_ai_new () {
	struct entity_ai res = {
		._radius = 10,
		.x = y = 0;
		.dirNormX = 1;
		.dirNormY = 0;
	};
	return res;
}

void entity_ai_buildApproximation (struct entity_ai *entity_ai) {
	(*entity_ai)._approximateObject = object_new object();
	(*entity_ai)._approximateObject.matrix.translate(x, y);
	struct float_arr coordinates = float_arr_create (8);
	(*entity_ai)._approximateObject.coordinates = coordinates;

	if ((*entity_ai)._radius == 0) return;

	for (int i = 0; i < NUM_SEGMENTS; i++) {
		push_float_arr (&coordinates, (*entity_ai)._radius * cos (2 *
					M_PI * i / NUM_SEGMENTS));
		push-float_arr (&coordinates.push, (*entity_ai)._radius * Math.sin (2 *
					M_PI * i / NUM_SEGMENTS));
		push-float_arr (&coordinates.push, (*entity_ai)._radius * Math.cos (2 *
					M_PI * (i + 1) / NUM_SEGMENTS));
		push-float_arr (&coordinates.push, (*entity_ai)._radius * Math.sin (2 *
					M_PI * (i + 1) / NUM_SEGMENTS));
	}
}

struct object entity_ai_get_approximateObject (struct entity_ai *entity_ai) {
	(*entity_ai)._approximateObject.matrix.identity();
	(*entity_ai)._approximateObject.matrix.translate(x, y);
	return _approximateObject;
}

float entity_ai_get_radius(struct entity_ai *entity_ai) {
	return (*entity_ai)._radius;
}

float entity_ai_get_radiusSquared (struct entity_ai *entity_ai) {
	return (*entity_ai)._radiusSquared;
}

float entity_ai_set_radius (struct entity_ai *entity_ai, float value) {
	(*entity_ai)._radius = value;
	(*entity_ai)._radiusSquared = (*entity_ai)._radius * (*entity_ai)._radius;
	return value;
}
