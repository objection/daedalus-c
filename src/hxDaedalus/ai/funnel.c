#include "funnel.h"

struct funnel funnel_new () {
	struct funnel res = {
		._numSamplesCircle = 16,
		._sampleCircle = point2d_arr_create (8),
		._poolPointSize = 3000,
	};
	assert (res._sampleCircle.d);
	res._poolPoints = point2d_arr_create (8);
	assert (res._poolPoints.d);
	for (int i = 0; i < res._poolPointSize; i++)
		point2d_arr_push (&res._poolPoints, point2d_new ());
}

void funnel_dispose (struct funnel *funnel) {
	(*funnel)._sampleCircle = null;
}

struct point2d getPoint (struct funnel *funnel, float x, float y) {
	(*funnel).__point = _poolPoints[_currPoolPointsIndex];
	(*funnel).__point.setXY (x, y);

	(*funnel)._currPoolPointsIndex++;
	if ((*funnel)._currPoolPointsIndex == (*funnel)._poolPointsSize) {
		push_point2d_arr (&(*funnel)._poolPoints, point2d_new ());
		(*funnel)._poolPointsSize++;
	}

	return (*funnel).__point;
}

//  COME BACK. I'd normally have the funnel as first argument but this
//  function doesn't use it.
struct point2d funnel_getCopyPoint  (struct point2d pointToCopy) {
	return funnel_getPoint (pointToCopy.x, pointToCopy.y);
}

float funnel_get_radius (struct funnel *funnel) {
	return (*funnel)._radius;
}

float funnel_set_radius (struct funnel *funnel, float : value) {
	(*funnel)._radius = max (0, value);
	(*funnel)._radiusSquared = _radius * _radius;
	(*funnel)._sampleCircle = new Array<Point2D>();
	if ((*funnel).radius == 0) return 0;
	for (int i = 0; i < (*funnel)._numSamplesCircle; i++) {
		point2d_arr_push (&(*funnel)._sampleCircle, point2d_new
				((*funnel)._radius * cos (-2 * M_PI * i /
					(*funnel)._numSamplesCircle),
				 (*funnel)._radiu4 * sin (-2 * M_PI * i /
					 (*funnel)._numSamplesCircle));
	}
	(*funnel)._sampleCircleDistanceSquared =
		((*funnel)._sampleCircle[0].x - (*funnel)._sampleCircle[1].x) *
		((*funnel)._sampleCircle[0].x - (*funnel)._sampleCircle[1].x) +
		((*funnel)._sampleCircle[0].y - (*funnel)._sampleCircle[1].y) *
		((*funnel)._sampleCircle[0].y - (*funnel)._sampleCircle[1].y);
	return (*funnel)._radius;
}


float findPath (struct funnel *funnel, float fromX, float fromY, float toX,
	float toY, struct face_arr listFaces, struct edge_arr listEdges,
	struct float_arr resultPath) {
	(*funnel)._currPoolPointsIndex = 0;

	// we check the start and goal
	if ((*funnel)._radius > 0) {
		struct face *checkFace = &listFaces.d[0];
		float distanceSquared = 0;
		float distance = 0;
		struct point2d p1 = checkFace.edge.originVertex.pos;
		struct point2d p2 = checkFace.edge.destinationVertex.pos;
		struct point2d p3 = checkFace.edge.nextLeftEdge.destinationVertex.pos;
		distanceSquared = (p1.x - fromX) * (p1.x - fromX) + (p1.y - fromY) *
			(p1.y - fromY);
		if (distanceSquared <= (*funnel)._radiusSquared) {
			distance = sqrt (distanceSquared);
			fromX = (*funnel)._radius * 1.01 * ((fromX - p1.x) / distance) +
				p1.x;
			fromY = (*funnel)._radius * 1.01 * ((fromY - p1.y) / distance) +
				p1.y;
		} else {
			distanceSquared = (p2.x - fromX) * (p2.x - fromX) + (p2.y - fromY)
				* (p2.y - fromY);
			if (distanceSquared <= (*funnel)._radiusSquared) {

				distance = sqrt (distanceSquared);
				fromX = (*funnel)._radius * 1.01 * ((fromX - p2.x) / distance) + p2.x;
				fromY = (*funnel)._radius * 1.01 * ((fromY - p2.y) / distance) + p2.y;
			}
			else {
				distanceSquared = (p3.x - fromX) * (p3.x - fromX) + (p3.y - fromY) * (p3.y - fromY);
				if (distanceSquared <= (*funnel)._radiusSquared) {

					distance = sqrt (distanceSquared);
					fromX = (*funnel)._radius * 1.01 * ((fromX - p3.x) / distance) + p3.x;
					fromY = (*funnel)._radius * 1.01 * ((fromY - p3.y) / distance) + p3.y;
				}
			}
		}  //

		checkFace = &listFaces[listFaces.n - 1];
		p1 = checkFace.edge.originVertex.pos;
		p2 = checkFace.edge.destinationVertex.pos;
		p3 = checkFace.edge.nextLeftEdge.destinationVertex.pos;
		distanceSquared = (p1.x - toX) * (p1.x - toX) + (p1.y - toY) * (p1.y -
				toY);
		if (distanceSquared <= (*funnel)._radiusSquared)
		{
			distance = sqrt (distanceSquared);
			toX = (*funnel)._radius * 1.01 * ((toX - p1.x) / distance) + p1.x;
			toY = (*funnel)._radius * 1.01 * ((toY - p1.y) / distance) + p1.y;
		}
		else
		{
			distanceSquared = (p2.x - toX) * (p2.x - toX) + (p2.y - toY) * (p2.y - toY);
			if (distanceSquared <= (*funnel)._radiusSquared)
			{
				distance = sqrt (distanceSquared);
				toX = (*funnel)._radius * 1.01 * ((toX - p2.x) / distance) + p2.x;
				toY = (*funnel)._radius * 1.01 * ((toY - p2.y) / distance) + p2.y;
			}
			else
			{
				distanceSquared = (p3.x - toX) * (p3.x - toX) + (p3.y - toY) * (p3.y - toY);
				if (distanceSquared <= (*funnel)._radiusSquared)
				{
					distance = sqrt (distanceSquared);
					toX = (*funnel)._radius * 1.01 * ((toX - p3.x) / distance) + p3.x;
					toY = (*funnel)._radius * 1.01 * ((toY - p3.y) / distance) + p3.y;
				}
			}
		}
	}  // we build starting and ending points



	struct point2d startPoint = point2d_new (fromX, fromY);
	struct point2d endPoint = new_point2d (toX, toY);

	// useful
	int i;
	int j;
	int k;
	struct edge *currEdge = NULL;
	struct vertex *currVertex = NULL;
	int direction;

	// fix for issue76
	if (listFaces.n > 1) {
		// first we skip the first face and first edge if the starting point
		// lies on the first interior edge:
		switch (geom2d_isInFace (fromX, fromY, listFaces[0]))
		{
			case EEdge (edge):
				if ( listEdges[0] == edge )
				{
					listEdges.shift ();
					listFaces.shift ();
				}
			case _:
				//
		}
	}
	if (listFaces.length == 1)
	{
		resultPath.push (startPoint.x);
		resultPath.push (startPoint.y);
		resultPath.push (endPoint.x);
		resultPath.push (endPoint.y);
		return;
	}

	// our funnels, inited with starting point
	var funnelLeft = new Array<Point2D>();
	var funnelRight = new Array<Point2D>();
	funnelLeft.push (startPoint);
	funnelRight.push (startPoint);

	// useful to keep track of done vertices and compare the sides
	var verticesDoneSide = new Map<Vertex,Int>();

	// we extract the vertices positions and sides from the edges list
	var pointsList = new Array<Point2D>();
	var pointSides = new Map<Point2D,Int>();
	// we keep the successor relation in a dictionnary
	var pointSuccessor = new Map<Point2D,Point2D>();
	//
	pointSides[ startPoint ] = 0;
	// we begin with the vertices in first edge
	currEdge = listEdges[0];
	var relativPos : Int = Geom2D.getRelativePosition2 (fromX, fromY, currEdge);
	var prevPoint : Point2D;
	var newPointA : Point2D;
	var newPointB : Point2D;
	newPointA = getCopyPoint ( currEdge.destinationVertex.pos );
	newPointB = getCopyPoint ( currEdge.originVertex.pos );

	pointsList.push (newPointA);
	pointsList.push (newPointB);
	pointSuccessor[ startPoint ] = newPointA;
	pointSuccessor[ newPointA ] = newPointB;
	prevPoint = newPointB;
	if ( relativPos == 1 ){
		pointSides[ newPointA ] = 1;
		pointSides[ newPointB ] = -1;
		verticesDoneSide[ currEdge.destinationVertex ] = 1;
		verticesDoneSide[ currEdge.originVertex ] = -1;
	}else if (relativPos == -1) {// then we iterate through the edges
		pointSides[ newPointA ] = -1;
		pointSides[ newPointB ] = 1;
		verticesDoneSide[ currEdge.destinationVertex ] = -1;
		verticesDoneSide[ currEdge.originVertex ] = 1;
	}



	var fromVertex = listEdges[ 0 ].originVertex;
	var fromFromVertex = listEdges[ 0 ].destinationVertex;
	for (i in 1...listEdges.length ){
		// we identify the current vertex and his origin vertex
		currEdge = listEdges[ i ];
		if (currEdge.originVertex == fromVertex)
		{
			currVertex = currEdge.destinationVertex;
		}
		else if (currEdge.destinationVertex == fromVertex)
		{
			currVertex = currEdge.originVertex;
		}
		else if (currEdge.originVertex == fromFromVertex)
		{
			currVertex = currEdge.destinationVertex;
			fromVertex = fromFromVertex;
		}
		else if (currEdge.destinationVertex == fromFromVertex)
		{
			currVertex = currEdge.originVertex;
			fromVertex = fromFromVertex;
		}
		else
		{
			Debug.trace ("IMPOSSIBLE TO IDENTIFY THE VERTEX !!!");
		}

		newPointA = getCopyPoint (currVertex.pos);
		pointsList.push (newPointA);
		direction = - verticesDoneSide[ fromVertex ];
		pointSides[ newPointA ] = direction;
		pointSuccessor[ prevPoint ] = newPointA;
		verticesDoneSide[ currVertex ] = direction;
		prevPoint = newPointA;
		fromFromVertex = fromVertex;
		fromVertex = currVertex;
	}  // we then we add the end point

	pointSuccessor[ prevPoint ] = endPoint;
	pointSides[ endPoint ] = 0;

	/*
	   debugSurface.graphics.clear ();
	   debugSurface.graphics.lineStyle (1, 0x0000FF);
	   var ppp1:Point = startPoint;
	   var ppp2:Point = pointSuccessor[ppp1];
	   while (ppp2)
	   {
	   debugSurface.graphics.moveTo (ppp1.x, ppp1.y+2);
	   debugSurface.graphics.lineTo (ppp2.x, ppp2.y+2);
	   debugSurface.graphics.drawCircle (ppp2.x, ppp2.y, 3);
	   ppp1 = ppp2;
	   ppp2 = pointSuccessor[ppp2];
	   }

	   debugSurface.graphics.lineStyle (1, 0x00FF00);
	   for (i=1 ; i<pointsList.length ; i++)
	   {
	   debugSurface.graphics.moveTo (pointsList[i-1].x+2, pointsList[i-1].y);
	   debugSurface.graphics.lineTo (pointsList[i].x+2, pointsList[i].y);
	   }
	   */

	// we will keep the points and funnel sides of the optimized path
	var pathPoints = new Array<Point2D>();
	var pathSides = new Map<Point2D,Int>();
	pathPoints.push ( startPoint );
	pathSides[ startPoint ] = 0;

	// now we process the points by order
	var currPos : Point2D;
	for (i in 0...pointsList.length){
		currPos = pointsList[i];

		// we identify the current vertex funnel's position by the position of his origin vertex
		if ( pointSides[ currPos ] == -1 ) {
			// current vertex is at right
			//Debug.trace ("current vertex is at right");
			j = funnelLeft.length - 2;
			while (j >= 0){
				direction = Geom2D.getDirection (funnelLeft[j].x, funnelLeft[j].y, funnelLeft[j + 1].x, funnelLeft[j + 1].y, currPos.x, currPos.y);
				if (direction != -1)
				{
					//Debug.trace ("funnels are crossing");

					funnelLeft.shift ();
					for (k in 0...j){
						pathPoints.push (funnelLeft[0]);
						pathSides[ funnelLeft[0] ] = 1;
						funnelLeft.shift ();
					}
					pathPoints.push (funnelLeft[0]);
					pathSides[ funnelLeft[0]] = 1;
					funnelRight.splice (0, funnelRight.length);
					funnelRight.push (funnelLeft[0] );
					funnelRight.push ( currPos );
					break;
				}
				j--;
			}

			funnelRight.push (currPos);
			j = funnelRight.length - 3;
			while (j >= 0){
				direction = Geom2D.getDirection (funnelRight[j].x, funnelRight[j].y, funnelRight[j + 1].x, funnelRight[j + 1].y, currPos.x, currPos.y);
				if (direction == -1)
					break
				else
				{
					funnelRight.splice (j + 1, 1);
				}
				j--;
			}
		}
		else
		{
			// current vertex is at left
			j = funnelRight.length - 2;
			while (j >= 0){
				direction = Geom2D.getDirection (funnelRight[j].x, funnelRight[j].y, funnelRight[j + 1].x, funnelRight[j + 1].y, currPos.x, currPos.y);
				if (direction != 1)
				{
					funnelRight.shift ();
					for (k in 0...j){
						pathPoints.push (funnelRight[0]);
						pathSides[funnelRight[0] ] = -1;
						funnelRight.shift ();
					}
					pathPoints.push (funnelRight[0]);
					pathSides[funnelRight[0] ]= -1;
					funnelLeft.splice (0, funnelLeft.length);
					funnelLeft.push (funnelRight[0] );
					funnelLeft.push ( currPos );
					break;
				}
				j--;
			}

			funnelLeft.push (currPos);
			j = funnelLeft.length - 3;
			while (j >= 0){
				direction = Geom2D.getDirection (funnelLeft[j].x, funnelLeft[j].y, funnelLeft[j + 1].x, funnelLeft[j + 1].y, currPos.x, currPos.y);
				if (direction == 1)
					break
				else
				{
					funnelLeft.splice (j + 1, 1);
				}
				j--;
			}
		}
	}  // check if the goal is blocked by one funnel's right vertex



	var blocked = false;
	//Debug.trace ("check if the goal is blocked by one funnel right vertex");
	j = funnelRight.length - 2;
	while (j >= 0){
		direction = Geom2D.getDirection (funnelRight[j].x, funnelRight[j].y, funnelRight[j + 1].x, funnelRight[j + 1].y, toX, toY);
		//Debug.trace ("dir" + funnelRight[j].x + "," + funnelRight[j].y + " " + funnelRight[j+1].x + "," + funnelRight[j+1].y + " " + toX + "," + toY);
		if (direction != 1)
		{
			//Debug.trace ("goal access right blocked");
			// access blocked
			funnelRight.shift ();
			for (k in 0...j + 1){
				pathPoints.push (funnelRight[0]);
				pathSides[ funnelRight[0] ] = -1;
				funnelRight.shift ();
			}
			pathPoints.push (endPoint);
			pathSides[ endPoint ] = 0;
			blocked = true;
			break;
		}
		j--;
	}

	if (!blocked)
	{
		// check if the goal is blocked by one funnel's left vertex
		//Debug.trace ("check if the goal is blocked by one funnel left vertex");
		j = funnelLeft.length - 2;
		while (j >= 0){
			direction = Geom2D.getDirection (funnelLeft[j].x, funnelLeft[j].y, funnelLeft[j + 1].x, funnelLeft[j + 1].y, toX, toY);
			//Debug.trace ("dir " + funnelLeft[j].x + "," + funnelLeft[j].y + " " + funnelLeft[j+1].x + "," + funnelLeft[j+1].y + " " + toX + "," + toY);
			if (direction != -1)
			{
				//Debug.trace ("goal access left blocked");
				// access blocked
				funnelLeft.shift ();
				for (k in 0...j + 1){
					pathPoints.push (funnelLeft[0]);
					pathSides[ funnelLeft[0] ] = 1;
					funnelLeft.shift ();
				}

				pathPoints.push (endPoint);
				pathSides[ endPoint ] = 0;
				blocked = true;
				break;
			}
			j--;
		}
	}  // if not blocked, we consider the direct path



	if (!blocked)
	{
		pathPoints.push (endPoint);
		pathSides[ endPoint ] = 0;
		blocked = true;
	}  // if radius is non zero


	var adjustedPoints= new Array<Point2D>();
	if (radius > 0)
	{

		var newPath : Array<Point2D> = new Array<Point2D>();

		if (pathPoints.length == 2)
		{
			adjustWithTangents (pathPoints[0], false, pathPoints[1], false, pointSides, pointSuccessor, newPath, adjustedPoints);
		}
		else if (pathPoints.length > 2)
		{
			// tangent from start point to 2nd point
			adjustWithTangents (pathPoints[0], false, pathPoints[1], true, pointSides, pointSuccessor, newPath, adjustedPoints);

			// tangents for intermediate points
			if (pathPoints.length > 3)
			{
				for (i in 1...pathPoints.length - 3 + 1){
					adjustWithTangents (pathPoints[i], true, pathPoints[i + 1], true, pointSides, pointSuccessor, newPath, adjustedPoints);
				}
			}  // tangent from last-1 point to end point



			var pathLength : Int = pathPoints.length;
			adjustWithTangents (pathPoints[pathLength - 2], true, pathPoints[pathLength - 1], false, pointSides, pointSuccessor, newPath, adjustedPoints);
		}

		newPath.push (endPoint);

		// adjusted path can have useless tangents, we check it
		checkAdjustedPath (newPath, adjustedPoints, pointSides);

		var smoothPoints = new Array<Point2D>();
		i = newPath.length - 2;
		while (i >= 1){
			smoothAngle (adjustedPoints[i * 2 - 1], newPath[i], adjustedPoints[i * 2], pointSides[ newPath[i] ], smoothPoints);
			while (smoothPoints.length != 0)
			{
				var temp = i * 2;
				adjustedPoints.splice (temp, 0);
				adjustedPoints.insert ( temp, smoothPoints.pop ());
			}
			i--;
		}
	}
	else
	{
		adjustedPoints = pathPoints;
	}  // extract coordinates



	for (i in 0...adjustedPoints.length){
		resultPath.push (adjustedPoints[i].x);
		resultPath.push (adjustedPoints[i].y);
	}
}
