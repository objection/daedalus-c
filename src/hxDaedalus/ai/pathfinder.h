#pragma once
/* package hxDaedalus.ai; */

#include "../data/edge.h"
#include "../data/face.h"
#include "../data/mesh.h"
#include "../data/math/geom2d.h"
#include "../debug/debug.h"


class pathfinder {

    struct entity_ai entity_ai;
    struct mesh mesh; // ( get, set )
    struct mesh _mesh;
    struct a_star a_star;
    struct funnel funnel;
    float radius;
    struct face_arr listFaces;
    struct edge_arr listEdges;

};
