#pragma once
/* package hxDaedalus.ai; */


#include "../data/constants.h"
#include "../data/edge.h"
#include "../data/face.h"
#include "../data/math.Geom2D"
#include "../data/Vertex"
#include "../data/math/point2d.h"
#include "../debug/debug.h"

struct funnel {
    float radius; // (get, set)


     float _radius;
     float _radiusSquared;
     float _numSamplesCircle;
     struct point2d_arr _sampleCircle;
     float _sampleCircleDistanceSquared;
     //public var debugSurface : Sprite;

     int _poolPointsSize;
     struct point2d_arr _poolPoints;
     int _currPoolPointsIndex;
     struct point2d __point;



     function adjustWithTangents(p1 : Point2D, applyRadiusToP1 : Bool, p2 : Point2D, applyRadiusToP2 : Bool, pointSides : Map<Point2D,Int>, pointSuccessor : Map<Point2D,Point2D>, newPath : Array<Point2D>, adjustedPoints : Array<Point2D>) : Void
    {
        // we find the tangent T between the points pathPoints[i] - pathPoints[i+1]
        // then we check the unused intermediate points between pathPoints[i] and pathPoints[i+1]
        // if a point P is too close from the segment, we replace T by 2 tangents T1, T2, between the points pathPoints[i] P and P - pathPoints[i+1]

        //Debug.trace("adjustWithTangents");

        var tangentsResult : Array<Float> = new Array<Float>();

        var side1 : Int = pointSides[ p1 ];
        var side2 : Int = pointSides[ p2 ];

        var pTangent1 : Point2D = null;
        var pTangent2 : Point2D = null;

        // if no radius application
        if (!applyRadiusToP1 && !applyRadiusToP2)
        {
            //Debug.trace("no radius applied");
            pTangent1 = p1;
            pTangent2 = p2;
        }
        // we apply radius to p2 only
        else if (!applyRadiusToP1)
        {
            //Debug.trace("! applyRadiusToP1");
            if (Geom2D.tangentsPointToCircle(p1.x, p1.y, p2.x, p2.y, _radius, tangentsResult)) {
				// p2 lies on the left funnel
				if (side2 == 1)
				{
					pTangent1 = p1;
					pTangent2 = getPoint(tangentsResult[2], tangentsResult[3]);
				}
				// p2 lies on the right funnel
				else
				{
					pTangent1 = p1;
					pTangent2 = getPoint(tangentsResult[0], tangentsResult[1]);
				}
			} else {
				Debug.trace("NO TANGENT");
				return;
			}
        }
        // we apply radius to p1 only
        else if (!applyRadiusToP2)
        {
            //Debug.trace("! applyRadiusToP2");
            if (Geom2D.tangentsPointToCircle(p2.x, p2.y, p1.x, p1.y, _radius, tangentsResult)) {
				if (tangentsResult.length > 0)
				{
					// p1 lies on the left funnel
					if (side1 == 1)
					{
						pTangent1 = getPoint(tangentsResult[0], tangentsResult[1]);
						pTangent2 = p2;
					}
					// p1 lies on the right funnel
					else
					{
						pTangent1 = getPoint(tangentsResult[2], tangentsResult[3]);
						pTangent2 = p2;
					}
				}
			} else {
				Debug.trace("NO TANGENT");
				return;
			}
        }
        // we apply radius to both points
        else
        {
            //Debug.trace("we apply radius to both points");
            // both points lie on left funnel
            if (side1 == 1 && side2 == 1)
            {
                Geom2D.tangentsParalCircleToCircle(_radius, p1.x, p1.y, p2.x, p2.y, tangentsResult);
                // we keep the points of the right tangent
                pTangent1 = getPoint(tangentsResult[2], tangentsResult[3]);
                pTangent2 = getPoint(tangentsResult[4], tangentsResult[5]);
            }
            // both points lie on right funnel
            else if (side1 == -1 && side2 == -1)
            {
                Geom2D.tangentsParalCircleToCircle(_radius, p1.x, p1.y, p2.x, p2.y, tangentsResult);
                // we keep the points of the left tangent
                pTangent1 = getPoint(tangentsResult[0], tangentsResult[1]);
                pTangent2 = getPoint(tangentsResult[6], tangentsResult[7]);
            }
            // 1st point lies on left funnel, 2nd on right funnel
            else if (side1 == 1 && side2 == -1)
            {
                if (Geom2D.tangentsCrossCircleToCircle(_radius, p1.x, p1.y, p2.x, p2.y, tangentsResult))
                {
                    // we keep the points of the right-left tangent
                    pTangent1 = getPoint(tangentsResult[2], tangentsResult[3]);
                    pTangent2 = getPoint(tangentsResult[6], tangentsResult[7]);
                }
                else
                {
                    // NO TANGENT BECAUSE POINTS TOO CLOSE
                    // A* MUST CHECK THAT !
                    Debug.trace("NO TANGENT, points are too close for radius");
                    return;
                }
            }
            // 1st point lies on right funnel, 2nd on left funnel
            else
            {
                if (Geom2D.tangentsCrossCircleToCircle(_radius, p1.x, p1.y, p2.x, p2.y, tangentsResult))
                {
                    // we keep the points of the left-right tangent
                    pTangent1 = getPoint(tangentsResult[0], tangentsResult[1]);
                    pTangent2 = getPoint(tangentsResult[4], tangentsResult[5]);
                }
                else
                {
                    // NO TANGENT BECAUSE POINTS TOO CLOSE
                    // A* MUST CHECK THAT !
                    Debug.trace("NO TANGENT, points are too close for radius");
                    return;
                }
            }
        }

        var successor = pointSuccessor[ p1 ];
        var distance : Float;
        while (successor != p2)
        {
            distance = Geom2D.distanceSquaredPointToSegment(successor.x, successor.y, pTangent1.x, pTangent1.y, pTangent2.x, pTangent2.y);
            if (distance < _radiusSquared)
            {
                adjustWithTangents(p1, applyRadiusToP1, successor, true, pointSides, pointSuccessor, newPath, adjustedPoints);
                adjustWithTangents(successor, true, p2, applyRadiusToP2, pointSides, pointSuccessor, newPath, adjustedPoints);
                return;
            }
            else
            {
                successor = pointSuccessor[successor];
            }
        }  /*if ( adjustedPoints.length > 0 )
        {
        var distanceSquared:Number;
        var lastPoint:Point = adjustedPoints[adjustedPoints.length-1];
        distanceSquared = (lastPoint.x - pTangent1.x)*(lastPoint.x - pTangent1.x) + (lastPoint.y - pTangent1.y)*(lastPoint.y - pTangent1.y);
        if (distanceSquared <= QEConstants.EPSILON_SQUARED)
        {
        adjustedPoints.pop();
        adjustedPoints.push(pTangent2);
        return;
        }
        }*/    // we check distance in order to remove useless close points due to straight line subdivision





        adjustedPoints.push( pTangent1 );
        adjustedPoints.push( pTangent2 );
        newPath.push( p1 );
    }

     function checkAdjustedPath( newPath : Array<Point2D>, adjustedPoints : Array<Point2D>, pointSides : Map<Point2D,Int> ) : Void
    {

        var needCheck = true;

        var point0 : Point2D;
        var point0Side : Int;
        var point1 : Point2D;
        var point1Side : Int;
        var point2 : Point2D;
        var point2Side : Int;

        var pt1 : Point2D;
        var pt2 : Point2D;
        var pt3 : Point2D;
        var dot : Float;

        var tangentsResult = new Array<Float>();
        var pTangent1 : Point2D = null;
        var pTangent2 : Point2D = null;

        while (needCheck)
        {
            needCheck = false;
            var i = 2;
            while(i < newPath.length ){
                point2 = newPath[i];
                point2Side = pointSides[ point2 ];
                point1 = newPath[i - 1];
                point1Side = pointSides[ point1 ];
                point0 = newPath[i - 2];
                point0Side = pointSides[ point0 ];

                if( point1Side == point2Side )
                {
                    pt1 = adjustedPoints[(i - 2) * 2];
                    pt2 = adjustedPoints[(i - 1) * 2 - 1];
                    pt3 = adjustedPoints[(i - 1) * 2];
                    dot = (pt1.x - pt2.x) * (pt3.x - pt2.x) + (pt1.y - pt2.y) * (pt3.y - pt2.y);
                    if (dot > 0)
                    {
                        //needCheck = true;
                        //Debug.trace("dot > 0");
                        // rework the tangent
                        if (i == 2)
                        {
                            // tangent from start point
                            Geom2D.tangentsPointToCircle(point0.x, point0.y, point2.x, point2.y, _radius, tangentsResult);
                            // p2 lies on the left funnel
                            if (point2Side == 1)
                            {
                                pTangent1 = point0;
                                pTangent2 = getPoint(tangentsResult[2], tangentsResult[3]);
                            }
                            else
                            {
                                pTangent1 = point0;
                                pTangent2 = getPoint(tangentsResult[0], tangentsResult[1]);
                            }
                        }
                        else if (i == newPath.length - 1)
                        {
                            // tangent to end point
                            Geom2D.tangentsPointToCircle(point2.x, point2.y, point0.x, point0.y, _radius, tangentsResult);
                            // p1 lies on the left funnel
                            if (point0Side == 1)
                            {
                                pTangent1 = getPoint(tangentsResult[0], tangentsResult[1]);
                                pTangent2 = point2;
                            }
                            // p1 lies on the right funnel
                            else
                            {
                                pTangent1 = getPoint(tangentsResult[2], tangentsResult[3]);
                                pTangent2 = point2;
                            }
                        }
                        else
                        {
                            // 1st point lies on left funnel, 2nd on right funnel
                            if (point0Side == 1 && point2Side == -1)
                            {
                                //Debug.trace("point0Side == 1 && point2Side == -1");
                                Geom2D.tangentsCrossCircleToCircle(_radius, point0.x, point0.y, point2.x, point2.y, tangentsResult);  // we keep the points of the right-left tangent  ;

                                pTangent1 = getPoint(tangentsResult[2], tangentsResult[3]);
                                pTangent2 = getPoint(tangentsResult[6], tangentsResult[7]);
                            }
                            // 1st point lies on right funnel, 2nd on left funnel
                            else if (point0Side == -1 && point2Side == 1)
                            {
                                //Debug.trace("point0Side == -1 && point2Side == 1");
                                Geom2D.tangentsCrossCircleToCircle(_radius, point0.x, point0.y, point2.x, point2.y, tangentsResult);  // we keep the points of the right-left tangent  ;

                                pTangent1 = getPoint(tangentsResult[0], tangentsResult[1]);
                                pTangent2 = getPoint(tangentsResult[4], tangentsResult[5]);
                            }
                            // both points lie on left funnel
                            else if (point0Side == 1 && point2Side == 1)
                            {
                                //Debug.trace("point0Side == 1 && point2Side == 1");
                                Geom2D.tangentsParalCircleToCircle(_radius, point0.x, point0.y, point2.x, point2.y, tangentsResult);
                                // we keep the points of the right tangent
                                pTangent1 = getPoint(tangentsResult[2], tangentsResult[3]);
                                pTangent2 = getPoint(tangentsResult[4], tangentsResult[5]);
                            }
                            // both points lie on right funnel
                            else if (point0Side == -1 && point2Side == -1)
                            {
                                //Debug.trace("point0Side == -1 && point2Side == -1");
                                Geom2D.tangentsParalCircleToCircle(_radius, point0.x, point0.y, point2.x, point2.y, tangentsResult);
                                // we keep the points of the right tangent
                                pTangent1 = getPoint(tangentsResult[0], tangentsResult[1]);
                                pTangent2 = getPoint(tangentsResult[6], tangentsResult[7]);
                            }
                        }
                        var temp = ( i - 2) * 2;
                        adjustedPoints.splice( temp, 1 );
                        adjustedPoints.insert( temp, pTangent1 );
                        temp = i * 2 - 1;
                        adjustedPoints.splice( temp, 1 );
                        adjustedPoints.insert( temp, pTangent2);

                        // delete useless point
                        newPath.splice(i - 1, 1);
                        adjustedPoints.splice((i - 1) * 2 - 1, 2);

                        tangentsResult.splice(0, tangentsResult.length);
                        i--;
                    }
                }
				i++;
            }
        }
    }

     function smoothAngle(prevPoint : Point2D, pointToSmooth : Point2D, nextPoint : Point2D, side : Int, encirclePoints : Array<Point2D>) : Void
    {
        var angleType = Geom2D.getDirection(prevPoint.x, prevPoint.y, pointToSmooth.x, pointToSmooth.y, nextPoint.x, nextPoint.y);

        /*
        Debug.trace("smoothAngle");
        Debug.trace("angleType " + angleType);
        Debug.trace("prevPoint " + prevPoint);
        Debug.trace("pointToSmooth " + pointToSmooth);
        Debug.trace("nextPoint " + nextPoint);
        */

        var distanceSquared = (prevPoint.x - nextPoint.x) * (prevPoint.x - nextPoint.x) + (prevPoint.y - nextPoint.y) * (prevPoint.y - nextPoint.y);
        if (distanceSquared <= _sampleCircleDistanceSquared)
            return;

        var index : Int = 0;
        var side1 : Int;
        var side2 : Int;
        var pointInArea : Bool;
        var xToCheck : Float;
        var yToCheck : Float;
        for (i in 0..._numSamplesCircle){
            pointInArea = false;
            xToCheck = pointToSmooth.x + _sampleCircle[i].x;
            yToCheck = pointToSmooth.y + _sampleCircle[i].y;
            side1 = Geom2D.getDirection(prevPoint.x, prevPoint.y, pointToSmooth.x, pointToSmooth.y, xToCheck, yToCheck);
            side2 = Geom2D.getDirection(pointToSmooth.x, pointToSmooth.y, nextPoint.x, nextPoint.y, xToCheck, yToCheck);

            // if funnel left
            if (side == 1)
            {
                //Debug.trace("funnel side is 1");
                // if angle is < 180
                if (angleType == -1)
                {
                    //Debug.trace("angle type is -1");
                    if (side1 == -1 && side2 == -1)
                        pointInArea = true;
                }
                // if angle is >= 180
                else
                {
                    //Debug.trace("angle type is 1")
                    if (side1 == -1 || side2 == -1)
                        pointInArea = true;
                }
            }
            // if funnel right
            else
            {
                // if angle is < 180
                if (angleType == 1)
                {
                    if (side1 == 1 && side2 == 1)
                        pointInArea = true;
                }
                // if angle is >= 180
                else
                {
                    if (side1 == 1 || side2 == 1)
                        pointInArea = true;
                }
            }
            if (pointInArea)
            {
                encirclePoints.splice(index, 0);
                encirclePoints.insert(index, new Point2D(xToCheck, yToCheck));
                index++;
            }
            else
            index = 0;
        }
        if (side == -1)
            encirclePoints.reverse();
    }
};
