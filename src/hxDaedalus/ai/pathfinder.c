#include "pathfinder.h"

struct pathfinder_new () {
	struct pathfinder res = {
		.a_star = a_star_new (),
		.funnel = funnel_new (),
		.listFaces = face_arr_create (8),
		.listEdges = edge_arr_create (8),
	};
	return res;
}

void pathfinder_dispose (struct pathfinder *pathfinder) {
	// COME BACK. Note sure if null, alloc, etc.
	assert (!"Put this back in");
#if 0
	(*pathfinder)._mesh = null;
	astar.dispose();
	astar = null;
	funnel.dispose();
	funnel = null;
	listEdges = null;
	listFaces = null;
#endif
}

struct mesh pathfinder_get_mesh (struct pathfinder *pathfinder) {
	// COME BACK. pointer, alloc, etc
	return (*pathfinder)._mesh;
}

struct mesh pathfinder_set_mesh (struct pathfinder *pathfinder, struct mesh value) {
	(*pathfinder)._mesh = value;
	(*pathfinder).astar.mesh = _mesh;
	return value;
}

void pathfinder_findPath(float toX, float toY, struct float_arr *result_path) {
	/* float_arr_remove (resultPath, 0, (*resultPath.length ); */
	(*result_path).n = 0;
	Debug.assertFalse(_mesh == null, "Mesh missing");
	Debug.assertFalse(entity == null, "Entity missing");

	if( Geom2D.isCircleIntersectingAnyConstraint( toX, toY, entity.radius, _mesh ) ) return;

	astar.radius = entity.radius;
	funnel.radius = entity.radius;

	listFaces.splice( 0, listFaces.length );
	listEdges.splice( 0, listEdges.length );
	astar.findPath( entity.x, entity.y, toX, toY, listFaces, listEdges );
	if( listFaces.length == 0 ){
		Debug.trace("PathFinder listFaces.length == 0");
		return;
	}

	funnel.findPath( entity.x, entity.y, toX, toY, listFaces, listEdges, resultPath );
}
