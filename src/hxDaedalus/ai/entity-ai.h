#pragma once
/* package hxDaedalus.ai; */


#include "../data/object.h"

struct entity_ai {
	struct object approximateObject; // (get, never)
	float dirNormY;
	float dirNormX;
	float y;
	float x;
	float radius; // (get, set)
	float radiusSquared; // (get, never)


	float _radius;
	float _radiusSquared;
	float _x;
	float _y;
	float _dirNormX;
	float _dirNormY;
	struct object _approximateObject;
	int NUM_SEGMENTS; // This is static inline = 6. It's in the .c file.
};

struct entity_ai entity_ai_new ();
void entity_ai_buildApproximation (struct entity_ai *entity_ai);
struct object entity_ai_get_approximateObject (struct entity_ai *entity_ai);
float entity_ai_get_radius(struct entity_ai *entity_ai);
float entity_ai_get_radiusSquared (struct entity_ai *entity_ai);
float entity_ai_set_radius (struct entity_ai *entity_ai, float value);
