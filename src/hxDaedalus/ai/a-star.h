#pragma once
/* package hxDaedalus.ai; */

#include "../data/edge.h"
#include "../data/face.h"
#include "../data/mesh.h"
#include "../data/vertex.h"
#include "../data/math/geom2d.h"
#include "../data/math/point2d.h"
#include "../iterators/from-face-to-inner-edges.h"
#include "../debug/debug.h"

// COMEBACK
// #include haxe.ds.StringMap;
// Better name?
struct face_n_float {
	struct face face;
	float val;
};

struct face_n_bool {
	struct face face;
	bool val;
};

struct a_star {

    float _radius;
    struct mesh _mesh;

	// COMEBACK
    struct face_n_bool closedFaces;
    struct face_arr sortedOpenedFaces;
    var openedFaces : Map<Face,Null<Bool>>;
	struct {
		struct face face;
		struct edge edge;
	} entryEdges;
	struct face_n_float entryX;
	struct face_n_float entryY;
	struct face_n_float scoreF;
	struct face_n_float scoreG;
	struct face_n_float scoreH;
	// COMEBACK. This could be a struct?
	struct face predecessor[2];

	struct FromFaceToInnerEdges iterEdge;

	float radiusSquared;
    float diameter;
    float diameterSquared;
    struct face fromFace;
    struct face toFace;
    struct face curFace;
};

float a_star_radius (get, set);
Mesh a_star_mesh (never, set);
void a_star_dispose (struct AStar *a_star);
float a_star_get_radius (struct AStar *a_star);
float a_star_set_radius (struct AStar *a_star, float value);
