#include "a-star.h"

struct a_star a_star_new (struct a_star *a_star) {
	struct a_star res = {
		// COME BACK. Probably have to malloc this
		.iterEdge = FromFaceToInnerEdges ();
	};
	return res;
}

void a_star_dispose (struct a_star *a_star) {
	(*a_star)._mesh = NULL;
	(*a_star).closedFaces = NULL;
	(*a_star).sortedOpenedFaces = NULL;
	(*a_star).openedFaces = NULL;
	(*a_star).entryEdges = NULL;
	(*a_star).entryX = NULL;
	(*a_star).entryY = NULL;
	(*a_star).scoreF = NULL;
	(*a_star).scoreG = NULL;
	(*a_star).scoreH = NULL;
	(*a_star).predecessor = NULL;
}

static float a_star_get_radius (struct a_star *a_star) {
	return (*a_star)._radius;
}

static float a_star_set_radius (struct a_star *a_star, float value) {
	(*a_star)._radius = value;
	(*a_star).radiusSquared = _radius * _radius;
	(*a_star).diameter = _radius * 2;
	(*a_star).diameterSquared = diameter * diameter;
	return value;
}

static Mesh a_star_set_mesh (struct a_star *a_star, Mesh value) {
	(*a_star)._mesh = value;
	return value;
}

void a_star_findPath (struct a_star *a_star, float fromX,  float fromY,
		float toX, float toY, face_Array resultListFaces,
		edge_Array resultListEdges) {
	//Debug.trace ("findPath");
	(*a_star).closedFaces = new Map<Face,Bool>();
	(*a_star).sortedOpenedFaces = new Array<Face>();
	(*a_star).openedFaces = new Map<Face,Bool>();
	(*a_star).entryEdges = new Map<Face,Edge>();
	(*a_star).entryX = new Map<Face,Float>();
	(*a_star).entryY = new Map<Face,Float>();
	(*a_star).scoreF = new Map<Face,Float>();
	(*a_star).scoreG = new Map<Face,Float>();
	(*a_star).scoreH = new Map<Face,Float>();
	(*a_star).predecessor = new Map<Face,Face>();

	Intersection loc;
	Edge locEdge;
	Vertex locVertex;
	float distance;
	Point2D p1;
	Point2D p2;
	Point2D p3;
	//
	loc = Geom2D.locatePosition (fromX, fromY, _mesh);
	switch (loc){
		case EVertex (vertex):
			locVertex = vertex;
			return;
		case EEdge (edge):
			locEdge = edge;
			if (locEdge.isConstrained) return;
			fromFace = locEdge.leftFace;
			break;
		case EFace (face):
			fromFace = face;
			break;
		case ENull:
		default: assert (0);
			//
	}

	loc = Geom2D.locatePosition (toX, toY, _mesh);
	switch (loc) {
		case EVertex ( vertex ):
			locVertex = vertex;
			toFace = locVertex.edge.leftFace;
			break;
		case EEdge ( edge ):
			locEdge = edge;
			toFace = locEdge.leftFace;
			break;
		case EFace ( face ):
			toFace = face;
			break;
		case ENull:
			break;
		default: assert (0);
	}
	/*
	   fromFace.colorDebug = 0xFF0000;
	   toFace.colorDebug = 0xFF0000;
	   Debug.trace ( "from face: " + fromFace );
	   Debug.trace ( "to face: " + toFace );
	*/

	sortedOpenedFaces.push (fromFace);
	entryEdges[fromFace] = null;
	entryX[fromFace] = fromX;
	entryY[fromFace] = fromY;
	scoreG[fromFace] = 0;
	var dist: Float = Math.sqrt ((toX - fromX) * (toX - fromX) + (toY - fromY) * (toY - fromY));
	scoreH[fromFace] = dist;
	scoreF[fromFace] = dist;

	var innerEdge : Edge;
	var neighbourFace : Face;
	var f : Float;
	var g : Float;
	var h : Float;
	var fromPoint = new Point2D ();
	var entryPoint = new Point2D ();
	var distancePoint = new Point2D ();
	var fillDatas : Bool;
	while ( true ){
		// no path found
		if ( sortedOpenedFaces.length == 0 ){
			Debug.trace ("a_star no path found");
			curFace = null;
			break;
		}  // we reached the target face

		curFace = sortedOpenedFaces.pop ();
		if ( curFace == toFace ) break;
		// we continue the search
		iterEdge.fromFace = curFace;
		while ( ( innerEdge = iterEdge.next () )!= null ) {
			if ( innerEdge.isConstrained ) continue;
			neighbourFace = innerEdge.rightFace;
			if ( !closedFaces[neighbourFace] ) {
				if (curFace != fromFace && _radius > 0 && !isWalkableByRadius ( entryEdges[curFace], curFace, innerEdge) ) {
					//                            Debug.trace ("- NOT WALKABLE -");
					//                            Debug.trace ( "from ", hxDaedalusEdge (__entryEdges[__curFace]).originVertex.id, hxDaedalusEdge (__entryEdges[__curFace]).destinationVertex.id );
					//                            Debug.trace ( "to", innerEdge.originVertex.id, innerEdge.destinationVertex.id );
					//                            Debug.trace ("----------------");
					continue;
				}

				fromPoint.x = entryX[curFace];
				fromPoint.y = entryY[curFace];

				// entryPoint will be the direct point of intersection between fromPoint and toXY if the edge innerEdge
				// intersects it
				var vw1 : Point2D = innerEdge.originVertex.pos;
				var vw2 : Point2D = innerEdge.destinationVertex.pos;
				if (!Geom2D.intersections2segments (fromPoint.x, fromPoint.y, toX, toY, vw1.x, vw1.y, vw2.x, vw2.y, entryPoint)) {
					// Recycle the entryPoint variable to create a Point2D (toX, toY)
					entryPoint.x = toX;
					entryPoint.y = toY;
					var vst = vw1.distanceSquaredTo (fromPoint) + vw1.distanceSquaredTo (entryPoint);
					var wst = vw2.distanceSquaredTo (fromPoint) + vw2.distanceSquaredTo (entryPoint);
					entryPoint.x = vst <= wst ? vw1.x : vw2.x;
					entryPoint.y = vst <= wst ? vw1.y : vw2.y;
				}

				distancePoint.x = entryPoint.x - toX;
				distancePoint.y = entryPoint.y - toY;
				h = distancePoint.length;
				distancePoint.x = fromPoint.x - entryPoint.x;
				distancePoint.y = fromPoint.y - entryPoint.y;
				g = scoreG[curFace] + distancePoint.length;
				f = h + g;
				fillDatas = false;
				if (openedFaces[neighbourFace] == null || !openedFaces[neighbourFace])
				{
					sortedOpenedFaces.push ( neighbourFace );
					openedFaces[neighbourFace] = true;
					fillDatas = true;
				}
				else if ( scoreF[neighbourFace] > f )
				{
					fillDatas = true;
				}
				if (fillDatas)
				{
					entryEdges[neighbourFace] = innerEdge;
					entryX[neighbourFace] = entryPoint.x;
					entryY[neighbourFace] = entryPoint.y;
					scoreF[neighbourFace] = f;
					scoreG[neighbourFace] = g;
					scoreH[neighbourFace] = h;
					predecessor[neighbourFace] = curFace;
				}
			}
		}  //

		openedFaces[curFace] = false;
		closedFaces[curFace] = true;
		sortedOpenedFaces.sort ( sortingFaces );
	}  // if we didn't find a path



	if (curFace == null)
		return;  // else we build the path  ;



	resultListFaces.push ( curFace );
	//curFace.colorDebug = 0x0000FF;
	while (curFace != fromFace) {
		resultListEdges.unshift ( entryEdges[curFace] );
		//entryEdges[__curFace].colorDebug = 0xFFFF00;
		//entryEdges[__curFace].oppositeEdge.colorDebug = 0xFFFF00;
		curFace = predecessor[curFace];
		//curFace.colorDebug = 0x0000FF;
		resultListFaces.unshift ( curFace );
	}
}

// faces with low distance value are at the end of the array
int sortingFaces (struct a_star *a_star, struct face a, struct face b) {
	if ((*a_star).scoreF[a] == (*a_star).scoreF[b]) return 0;
	else if ((*a_star).scoreF[a] < (*a_star).scoreF[b]) return 1;
	else return -1;
}

bool isWalkableByRadius (struct edge fromEdge,
		struct face throughFace, struct edge toEdge) {
	struct vectex vA = null;  // the vertex on fromEdge not on toEdge
	struct vectex vB = null;  // the vertex on toEdge not on fromEdge
	struct vertex vC = null;  // the common vertex of the 2 edges (pivot)

	// we identify the points
	if (fromEdge.originVertex == toEdge.originVertex)
	{
		vA = fromEdge.destinationVertex;
		vB = toEdge.destinationVertex;
		vC = fromEdge.originVertex;
	}
	else if (fromEdge.destinationVertex == toEdge.destinationVertex)
	{
		vA = fromEdge.originVertex;
		vB = toEdge.originVertex;
		vC = fromEdge.destinationVertex;
	}
	else if (fromEdge.originVertex == toEdge.destinationVertex)
	{
		vA = fromEdge.destinationVertex;
		vB = toEdge.originVertex;
		vC = fromEdge.originVertex;
	}
	else if (fromEdge.destinationVertex == toEdge.originVertex)
	{
		vA = fromEdge.originVertex;
		vB = toEdge.destinationVertex;
		vC = fromEdge.destinationVertex;
	}

	var dot : Float;
	var result : Bool;
	var distSquared : Float;

	// if we have a right or obtuse angle on CAB
	dot = (vC.pos.x - vA.pos.x) * (vB.pos.x - vA.pos.x) + (vC.pos.y - vA.pos.y) * (vB.pos.y - vA.pos.y);
	if( dot <= 0 ){
		// we compare length of AC with radius
		distSquared = (vC.pos.x - vA.pos.x) * (vC.pos.x - vA.pos.x) + (vC.pos.y - vA.pos.y) * (vC.pos.y - vA.pos.y);
		if (distSquared >= diameterSquared) {
			return true;
		} else {
			return false;
		}
	}  // if we have a right or obtuse angle on CBA



	dot = (vC.pos.x - vB.pos.x) * (vA.pos.x - vB.pos.x) + (vC.pos.y - vB.pos.y) * (vA.pos.y - vB.pos.y);
	if( dot <= 0 ) {
		// we compare length of BC with radius
		distSquared = (vC.pos.x - vB.pos.x) * (vC.pos.x - vB.pos.x) + (vC.pos.y - vB.pos.y) * (vC.pos.y - vB.pos.y);
		if (distSquared >= diameterSquared) {
			return true;
		} else {
			return false;
		}
	}  // we identify the adjacent edge (facing pivot vertex)



	var adjEdge : Edge;
	if( throughFace.edge != fromEdge && throughFace.edge.oppositeEdge != fromEdge && throughFace.edge != toEdge && throughFace.edge.oppositeEdge != toEdge) {
		adjEdge = throughFace.edge;
	} else if( throughFace.edge.nextLeftEdge != fromEdge && throughFace.edge.nextLeftEdge.oppositeEdge != fromEdge && throughFace.edge.nextLeftEdge != toEdge && throughFace.edge.nextLeftEdge.oppositeEdge != toEdge ){
		adjEdge = throughFace.edge.nextLeftEdge;
	} else {
		adjEdge = throughFace.edge.prevLeftEdge;
	}
	// if the adjacent edge is constrained, we check the distance of orthognaly projected
	if( adjEdge.isConstrained ){
		var proj = new Point2D( vC.pos.x, vC.pos.y );
		Geom2D.projectOrthogonaly( proj, adjEdge );
		distSquared = (proj.x - vC.pos.x) * (proj.x - vC.pos.x) + (proj.y - vC.pos.y) * (proj.y - vC.pos.y);
		if( distSquared >= diameterSquared ) {
			return true;
		} else {
			return false;
		}
	}
	// if the adjacent is not constrained
	else
	{
		var distSquaredA : Float = (vC.pos.x - vA.pos.x) * (vC.pos.x - vA.pos.x) + (vC.pos.y - vA.pos.y) * (vC.pos.y - vA.pos.y);
		var distSquaredB : Float = (vC.pos.x - vB.pos.x) * (vC.pos.x - vB.pos.x) + (vC.pos.y - vB.pos.y) * (vC.pos.y - vB.pos.y);
		if( distSquaredA < diameterSquared || distSquaredB < diameterSquared ){
			return false;
		} else {
			var vFaceToCheck = new Array<Face>();
			var vFaceIsFromEdge = new Array<Edge>();
			var facesDone = new Map<Face,Bool>();
			vFaceIsFromEdge.push(adjEdge);
			if( adjEdge.leftFace == throughFace ){
				vFaceToCheck.push(adjEdge.rightFace);
				facesDone[adjEdge.rightFace] = true;
			} else {
				vFaceToCheck.push(adjEdge.leftFace);
				facesDone[ adjEdge.leftFace ] = true;
			}

			var currFace : Face;
			var faceFromEdge : Edge;
			var currEdgeA : Edge;
			var nextFaceA : Face;
			var currEdgeB : Edge;
			var nextFaceB : Face;
			while( vFaceToCheck.length > 0 ){
				currFace = vFaceToCheck.shift();
				faceFromEdge = vFaceIsFromEdge.shift();


				if( currFace.edge == faceFromEdge || currFace.edge == faceFromEdge.oppositeEdge ){
					// we identify the 2 edges to evaluate
					currEdgeA = currFace.edge.nextLeftEdge;
					currEdgeB = currFace.edge.nextLeftEdge.nextLeftEdge;
				} else if( currFace.edge.nextLeftEdge == faceFromEdge || currFace.edge.nextLeftEdge == faceFromEdge.oppositeEdge ){
					// we identify the faces related to the 2 edges
					currEdgeA = currFace.edge;
					currEdgeB = currFace.edge.nextLeftEdge.nextLeftEdge;
				} else {
					currEdgeA = currFace.edge;
					currEdgeB = currFace.edge.nextLeftEdge;
				}

				if( currEdgeA.leftFace == currFace ){
					nextFaceA = currEdgeA.rightFace;
				} else {
					nextFaceA = currEdgeA.leftFace;
				}
				if( currEdgeB.leftFace == currFace ){
					nextFaceB = currEdgeB.rightFace;
				} else {
					nextFaceB = currEdgeB.leftFace;
				}
				// we check if the next face is not already in pipe
				// and if the edge A is close to pivot vertex
				if( !facesDone[ nextFaceA ] && Geom2D.distanceSquaredVertexToEdge( vC, currEdgeA ) < diameterSquared ){
					// if the edge is constrained
					if( currEdgeA.isConstrained ){
						// so it is not walkable
						return false;
					} else {
						// if the edge is not constrained, we continue the search
						vFaceToCheck.push(nextFaceA);
						vFaceIsFromEdge.push(currEdgeA);
						facesDone[ nextFaceA ] = true;
					}
				}  // and if the edge B is close to pivot vertex    // we check if the next face is not already in pipe

				if( !facesDone[ nextFaceB ] && Geom2D.distanceSquaredVertexToEdge(vC, currEdgeB) < diameterSquared ){
					// if the edge is constrained
					if( currEdgeB.isConstrained ){
						// so it is not walkable
						return false;
					} else {
						// if the edge is not constrained, we continue the search
						vFaceToCheck.push( nextFaceB );
						vFaceIsFromEdge.push( currEdgeB );
						facesDone[ nextFaceB ] = true;
					}
				}
			}  // if we didn't previously meet a constrained edge

			return true;
		}
	}

	return true;
}
