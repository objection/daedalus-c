#pragma once
/* package hxDaedalus.data; */
#include "../../global.h"
#include "math/matrix2d.h"
#include "constraint-shape.h"

struct object {
	int id; // (get, never) 
	float pivotX; // (get, set)
	float pivotY; // (get, set)
	float scaleX; // (get, set)
	float scaleY; // (get, set)
	float rotation; // (get, set)
	float x; // (get, set)
	float y; // (get, set)
	struct matrix2d matrix; // (get, set)
	struct float_arr coordinates; // (get, set)
	struct constraint_shape constraintShape; // (get, set)
	bool hasChanged; // (get, set)
	struct edge_arr edges; // (get, never)
	struct float_arr polyPoints; // (never,set)
	struct float_arr_arr multiPoints; // ( never, set ): Array<Array<Float>>;

	int INC;
	int _id;

	struct matrix2d _matrix;
	// COME BACK. Should this be a static array?
	struct float_arr _coordinates;
	struct constraint_shape _constraintShape;

	float _pivotX;
	float _pivotY;

	float _scaleX;
	float _scaleY;
	float _rotation;
	float _x;
	float _y;

	bool _hasChanged;
};
DECLARE_ARR (struct object, object);

struct object object_new ();
int object_get_id (struct object *object);
void object_dispose (struct object *object);
void object_updateValuesFromMatrix (struct object *object);
void object_updateMatrixFromValues (struct object *object);
float object_get_pivotX (struct object *object);
float object_set_pivotX (struct object *object, float value);
float object_get_pivotY (struct object *object);
float object_set_pivotY (struct object *object, float value);
float object_get_scaleX (struct object *object);
float object_set_scaleX (struct object *object, float value);
float object_get_scaleY (struct object *object);
float object_set_scaleY (struct object *object, float value);
float object_get_rotation (struct object *object);
float object_set_rotation (struct object *object, float value);
float object_get_x (struct object *object);
float object_set_x (struct object *object, float value );
float object_get_y (struct object *object);
float object_set_y (struct object *object, float value);
struct matrix2d object_get_matrix (struct object *object);
struct matrix2d object_set_matrix (struct object *object,
		struct matrix2d value);
struct float_arr *object_get_coordinates (struct object *object);
bool object_clockWise (struct object *object);
struct float_arr object_set_polyPoints (struct object *object,
		struct float_arr shape);
struct float_arr object_set_coordinates (struct object *object,
		struct float_arr value);
struct constraint_shape object_get_constraintShape (struct object *object);
struct constraint_shape object_set_constraintShape (struct object *object,
		struct constraint_shape value);
bool object_get_hasChanged (struct object *object);
bool object_set_hasChanged (struct object *object, bool value);
struct edge_arr object_get_edges (struct object *object);
