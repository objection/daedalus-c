#pragma once
/* package hxDaedalus.data; */
#include "constraint-segment.h"

struct constraint_shape {
    /* public var id( get, never ) : Int; */
    struct constraint_segment_arr *segments;
    int INC;
    int _id;
};
DECLARE_ARR (struct constraint_shape, constraint_shape);

struct constraint_shape constraint_shape_new ();
int constraint_shape_get_id (struct constraint_shape *constraint_shape);
void constraint_shape_dispose (struct constraint_shape *shape);
