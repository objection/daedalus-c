#pragma once
/* package hxDaedalus.data; */

#include "object.h"
#include "constraint-shape.h"
#include "vertex.h"
#include "math/geom2d.h"
#include "math/matrix2d.h"
#include "math/point2d.h"
#include "../iterators/from-mesh-to-vertices.h"
#include "../iterators/from-vertex-to-incoming-edges.h"
#include "../iterators/from-vertex-to-outgoing-edges.h"
#include "../debug/debug.h"

struct mesh {
	float height; // (get, never)
	float width; // (get, never)
	bool clipping; // (get, set)
	int id; // (get, never)
	struct constraint_shape_arr __constraintShapes; // (get, never)

	int _id;

	float _width;
	float _height;
	// COME BACK. This is set to 0 here and to 1 in _new. A mistake?
	bool _clipping;

	// COME BACK. These next seven were all "= null".
	// I doubt it matters, since the _new function creates the array.
	struct vertex_arr _vertices; 
	struct edge_arr _edges;
	struct face_arr _faces;
	struct constraint_shape_arr _constraintShapes;
	struct object_arr _objects;

	// keep references of center vertex and bounding edges when split, useful
	// to restore edges as Delaunay
	struct vertex __centerVertex;
	struct edge_arr __edgesToCheck;


	bool __objectsUpdateInProgress;

};

