#include "face.h"

// COME BACK. This was declared static in the class, which means it's
// "class local" rather than "class instance local". Ie, it's variable local
// to this C file??????????
int INC;

DEFINE_ARR (struct face, face);

struct face face_new (int id) {
	struct face res = {
		.colorDebug = -1,
		._id = id,
		.INC = INC++,
	};
}

int face_get_id (struct face *face) {
	return (*face)._id;
}

bool face_get_isReal (struct face *face) {
	return (*face)._isReal;
}

void face_set_datas (struct face *face, struct edge *edge) {
	(*face)._isReal = true;
	// COME BACK. Is edge a pointer? Malloc?
	(*face)._edge = edge;
}

// must use this if you want te set isReal to false
void face_setDatas (struct face *face, struct edge *edge, bool isReal) {
	(*face)._isReal = isReal;
	(*face)._edge = edge;
}

void face_dispose (struct face *face) {
	(*face)._edge = NULL;
}

struct edge *face_get_edge (struct face *face) {
	return (*face)._edge;
}
