#pragma once
/* package hxDaedalus.data; */

#include "../../global.h"
#include "edge.h"

struct edge;
struct edge_arr;
struct constraint_shape;

struct constraint_segment {
    int id; // (get, never)
    struct constriant_shape *fromShape;
    struct edge_arr *edges; // (get, never) 
    int INC;
    int _id;
    struct edge_arr *_edges;
};

DECLARE_ARR (struct constraint_segment, constraint_segment);

struct constraint_segment constriant_segment_new ();
int constraint_segment_get_id (struct constraint_segment *segment);
void constraint_segment_addEdge (struct constraint_segment *segment,
		struct edge *edge);
void constraint_segment_removeEdge (struct constraint_segment *segment,
		struct edge *edge);
struct edge_arr *constraint_segment_get_edges (struct constraint_segment
		*segment);
void constraint_segment_dispose (struct constraint_segment *segment);
