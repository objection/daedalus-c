#include "edge.h"
#include "constraint-segment.h"
#include <assert.h>

DEFINE_ARR (struct edge, edge);

int INC;

struct edge edge_new () {
	struct edge res = {
		._id = INC++,
		.colorDebug = -1,
		// Needs to be malloced. I suppose I could have you pas in one?
		/* .fromConstraintSegments = malloc (sizeof *res.fromConstraintSegments), */
	};
	*res.fromConstraintSegments = constraint_segment_arr_create (8);
	return res;
}

int edge_get_id (struct edge *edge) {
	return (*edge)._id;
}

bool edge_get_isReal (struct edge *edge) {
	return (*edge)._isReal;
}

bool edge_get_isConstrained (struct edge *edge) {
	return (*edge)._isConstrained;
}

// COME BACK. Are these parameters malloced?
void edge_setDatas (struct edge *edge, struct vertex *originVertex,
		struct edge *oppositeEdge, struct edge *nextLeftEdge,
		struct face *leftFace, bool isReal, bool isConstrained) {
	(*edge)._isConstrained = isConstrained;
	(*edge)._isReal = isReal;
	(*edge)._originVertex = originVertex;
	(*edge)._oppositeEdge = oppositeEdge;
	(*edge)._nextLeftEdge = nextLeftEdge;
	(*edge)._leftFace = leftFace;
}

void edge_addFromConstraintSegment (struct edge *edge,
		struct constraint_segment *segment) {
	if (!constraint_segment_arr_find ((*edge).fromConstraintSegments,
				segment))
		constraint_segment_arr_push ((*edge).fromConstraintSegments,
				*segment);
}

void edge_removeFromConstraintSegment (struct edge *edge,
		struct constraint_segment *segment) {
	struct constraint_segment *ret = constraint_segment_arr_find
		((*edge).fromConstraintSegments, segment);
	if (ret)
		constraint_segment_arr_remove ((*edge).fromConstraintSegments,
				ret - (*(*edge).fromConstraintSegments).d, 1);
}

void edge_dispose (struct edge *edge) {
	(*edge)._originVertex = NULL;
	(*edge)._oppositeEdge = NULL;
	(*edge)._nextLeftEdge = NULL;
	(*edge)._leftFace = NULL;
	(*edge).fromConstraintSegments = NULL;
}

struct vertex *edge_set_originVertex (struct edge *edge, struct vertex *value) {
	(*edge)._originVertex = value;
	return value;
}

struct edge *edge_set_nextLeftEdge (struct edge *edge, struct edge *value) {
	(*edge)._nextLeftEdge = value;
	return value;
}

struct face *edge_set_leftFace (struct edge *edge, struct face *value) {
	(*edge)._leftFace = value;
	return value;
}

bool edge_set_isConstrained (struct edge *edge, bool value) {
	(*edge)._isConstrained = value;
	return value;
}

struct vertex *edge_get_originVertex(struct edge *edge) {
	return (*edge)._originVertex;
}

struct vertex *edge_get_destinationVertex(struct edge *edge) {
	return (*(*edge).oppositeEdge).originVertex;
}

struct edge *edge_get_oppositeEdge (struct edge *edge) {
	return (*edge)._oppositeEdge;
}

struct edge *edge_get_nextLeftEdge (struct edge *edge) {
	return (*edge)._nextLeftEdge;
}

struct edge *edge_get_prevLeftEdge (struct edge *edge) {
	return (*(*edge)._nextLeftEdge).nextLeftEdge;
}

struct edge *edge_get_nextRightEdge(struct edge *edge) {
	return (*(*(*(*edge)._oppositeEdge).nextLeftEdge).nextLeftEdge).
		oppositeEdge;
}

struct edge *edge_get_prevRightEdge(struct edge *edge) {
	return (*(*(*edge)._oppositeEdge).nextLeftEdge).oppositeEdge;
}

struct edge *edge_get_rotLeftEdge(struct edge *edge) {
	return (*(*(*edge)._nextLeftEdge).nextLeftEdge).oppositeEdge;
}

struct edge *edge_get_rotRightEdge(struct edge *edge) {
	return (*(*edge)._oppositeEdge).nextLeftEdge;
}

struct face *edge_get_leftFace (struct edge *edge) {
	return (*edge)._leftFace;
}

struct face *edge_get_rightFace (struct edge *edge) {
	return (*(*edge)._oppositeEdge).leftFace;
}
struct triangle edge_get_triangle (struct edge *edge) {
	return triangle_new ((*edge).originVertex,
			(*(*edge).nextLeftEdge).originVertex,
			(*(*edge).nextLeftEdge).destinationVertex);
}

char *edge_toString (struct edge *edge) {
	assert (!"Not here");
	/* return "edge " + originVertex.id + " - " + destinationVertex.id; */
}
