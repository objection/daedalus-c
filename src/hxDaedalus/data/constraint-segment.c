#include "constraint-segment.h"

DEFINE_ARR (struct constraint_segment, constraint_segment);

struct constraint_segment constriant_segment_new (){
	struct constriant_segment res = {
		._id = INC++,
		._edges = create_edge_arr (8),
	};
}

int constraint_segment_get_id (struct constraint_segment *segment) {
	return (*segment)._id;
}

void constraint_segment_addEdge (struct constraint_segment *segment,
		struct edge edge) {
	if ((*segment)._edges.indexOf (edge) == -1
			&& (*segment)._edges.indexOf (edge.oppositeEdge) == -1 )
		(*segment)._edges.push (edge);
}

void constraint_segment_removeEdge (struct constraint_segment *segment,
		struct edge *edge) {
	int index;
	(*segment).index = (*segment)._edges.indexOf(edge);
	if ((*segment).index == -1) (*segment).index =
		(*segment)._edges.indexOf ((*segment).edge.oppositeEdge);
	if ((*segment).index != -1) (*segment)._edges.splice (index, 1);
}

struct edge_arr *constraint_segment_get_edges (struct constraint_segment
		*segment) {
	return (*segment)._edges;
}

void constraint_segment_dispose (struct constraint_segment *segment) {
	remove_edge_arr (&(*segment)._edges);
	(*segment).fromShape = null;
}

#if 0 // COME BACK. I don't think I'll put in a tostring function.
function toString() : String {
	return "seg_id " + _id;
}
#endif
