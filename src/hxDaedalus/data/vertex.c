#include "vertex.h"
#include <assert.h>

DEFINE_ARR (struct vertex, vertex);
int INC;
// NOTE: was public
struct vertex vertex_new () {
	struct vertex res = {
		._id = INC++,
		// COME BACK. Probably have to malloc this.
		._pos = {0},
		._fromConstraintSegments = malloc (sizeof *res._fromConstraintSegments),
		.colorDebug = -1,
		._edge = malloc (sizeof *res._edge),
	};
	*res._fromConstraintSegments = constraint_segment_arr_create (8);
	return res;
}

int vertex_get_id (struct vertex *vertex) {
	return (*vertex)._id;
}

bool vertex_get_isReal (struct vertex *vertex) {
	return (*vertex)._isReal;
}

struct point2d vertex_get_pos (struct vertex *vertex) {
	return (*vertex)._pos;
}

struct constraint_segment_arr *vertex_get_fromConstraintSegments (struct vertex
		*vertex) {
	return (*vertex)._fromConstraintSegments;
}

struct constraint_segment_arr *vertex_set_fromConstraintSegments (struct vertex
		*vertex, struct constraint_segment_arr *value) {
	return (*vertex)._fromConstraintSegments = value;
}

void vertex_setDatas (struct vertex *vertex, struct edge *edge, bool isReal) {
	(*vertex)._isReal = isReal;
	(*vertex)._edge = edge;
}

void vertex_addFromConstraintSegment (struct vertex *vertex,
		struct constraint_segment *segment) {
	if (!constraint_segment_arr_find ((*vertex)._fromConstraintSegments,
				segment))
		constraint_segment_arr_push ((*vertex)._fromConstraintSegments,
				*segment);
}

void vertex_removeFromConstraintSegment (struct vertex *vertex,
		struct constraint_segment *segment) {
	struct constraint_segment *ret = NULL;
	if (constraint_segment_arr_find ((*vertex)._fromConstraintSegments,
				segment))
		constraint_segment_arr_remove ((*vertex)._fromConstraintSegments,
				ret - (*(*vertex)._fromConstraintSegments).d, 1);
}

void vertex_dispose (struct vertex *vertex) {
	assert (!"malloced?");
	/* (*vertex)._pos = {0}; */
	(*vertex)._edge = NULL;
	(*vertex)._fromConstraintSegments = NULL;
}

struct edge *vertex_get_edge (struct vertex *vertex) {
	return (*vertex)._edge;
}

struct edge *set_edge (struct vertex *vertex, struct edge *value) {
	return (*vertex)._edge = value;
}

char *toString (struct vertex *vertex) {
	assert (!"Probably won't include this");
#if 0
	return "ver_id " + _id;
#endif
}
