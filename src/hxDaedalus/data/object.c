#include "object.h"
#include "../../global.h"

DEFINE_ARR (struct object, object);
int INC;

struct object object_new () {
	struct object res = {
		._id = INC++,

		._pivotX = 0,
		._pivotY = 0,

		// COME BACK. This is probably fine. It was new matrix. I guess
		// that means it's allocated. But I won't do that.
		._matrix = {0},
		._scaleX = 1,
		._scaleY = 1,
		._rotation = 0,
		._x = 0,
		._y = 0,
		._coordinates = float_arr_create (8),
		._hasChanged = false,
	};
	return res;
}

int object_get_id (struct object *object) {
	return (*object)._id;
}

void object_dispose (struct object *object) {
	// COME BACK. My _matrix right now isn't malloced, so no null needed.

	/* (*object)._matrix = null; */
	float_arr_delete (&(*object)._coordinates);
	// COME BACK. Same
	/* (*object)._constraintShape = null; */
}

void object_updateValuesFromMatrix (struct object *object) {
	// NOTE: this was blank in the original, too.

}

void object_updateMatrixFromValues (struct object *object) {
	struct matrix2d *matrix = &(*object)._matrix;
	matrix2d_identity (matrix);
	matrix2d_translate (matrix, -(*object)._pivotX, -(*object)._pivotY);
	matrix2d_scale (matrix, (*object)._scaleX, (*object)._scaleY);
	matrix2d_rotate (matrix, (*object)._rotation);
	matrix2d_translate (matrix, (*object)._x, (*object)._y);
}

float object_get_pivotX (struct object *object) {
	return (*object)._pivotX;
}

float object_set_pivotX (struct object *object, float value) {
	(*object)._pivotX = value;
	(*object)._hasChanged = true;
	return value;
}

float object_get_pivotY (struct object *object) {
	return (*object)._pivotY;
}

float object_set_pivotY (struct object *object, float value) {
	(*object)._pivotY = value;
	(*object)._hasChanged = true;
	return value;
}

float object_get_scaleX (struct object *object) {
	return (*object)._scaleX;
}

float object_set_scaleX (struct object *object, float value) {
	if ((*object)._scaleX != value ){
		(*object)._scaleX = value;
		(*object)._hasChanged = true;
	}
	return value;
}

float object_get_scaleY (struct object *object) {
	return (*object)._scaleY;
}

float object_set_scaleY (struct object *object, float value) {
	if ((*object)._scaleY != value ){
		(*object)._scaleY = value;
		(*object)._hasChanged = true;
	}
	return value;
}

float object_get_rotation (struct object *object) {
	return (*object)._rotation;
}

float object_set_rotation (struct object *object, float value) {
	if ((*object)._rotation != value ){
		(*object)._rotation = value;
		(*object)._hasChanged = true;
	}
	return value;
}

float object_get_x (struct object *object) {
	return (*object)._x;
}

float object_set_x (struct object *object, float value ) {
	if ((*object)._x != value ){
		(*object)._x = value;
		(*object)._hasChanged = true;
	}
	return value;
}

float object_get_y (struct object *object) {
	return (*object)._y;
}

float object_set_y (struct object *object, float value) {
	if ((*object)._y != value ) {
		(*object)._y = value;
		(*object)._hasChanged = true;
	}
	return value;
}

struct matrix2d object_get_matrix (struct object *object) {
	return (*object)._matrix;
}

struct matrix2d object_set_matrix (struct object *object,
		struct matrix2d value) {
	(*object)._matrix = value;
	(*object)._hasChanged = true;
	return value;
}

// COME BACK. I assume it should return a pointer because ...
struct float_arr *object_get_coordinates (struct object *object) {
	return &(*object)._coordinates;
}

// COME BACK: was public
bool object_clockWise (struct object *object) {
	int count = 0;
	// COME BACK: could just read the value directly.
	struct float_arr *c = object_get_coordinates (object);
	float x1;
	float y1;
	float x2;
	float y2;
	float signedArea2 = 0;
	size_t len = (*c).n;
	while (count < len) {
		// NOTE. This is embarassing. I'm trying to stick to how the original
		// authors did shit, because since I don't know haxe I'm worried I'll
		// fuck something up. But obviously using a pointer to object's
		// _coordinates is stupid.
		x1 = (*c).d[count];
		y1 = (*c).d[count + 1];
		x2 = (*c).d[count + 2];
		y2 = (*c).d[count + 3];
		count += 4;
		signedArea2 += (x1 * y2 - x2 * y1);
	}
	return signedArea2 > 0;
}

#if 0
// COME BACK. I have fucked up with function.
// I thought it might be an array of float arrays, but I think it's
// just a 2d array.
struct float_arr object_set_multiPoints (struct object *object,
		struct float_arr *arr) {
	struct float_arr shapeCoords = float_arr_create (8);
	int j;
	for (float *shape = (*arr).d;
			shape < (*arr).d + (*arr).n; shape++){
	/* for (shape in arr){ */
		float_arr_push (&shapeCoords, shape[0]);
		float_arr_push (&shapeCoords, shape[1]);
		for(int i  = 2; i < (*shape).n; i++) {
			if (i % 2 == 0 && i > 2 ){
				float_arr_push (&shapeCoords, shape[i - 2]);
				float_arr_push (&shapeCoords, shape[ i - 1 ]);
			}
			float_arr_push (&shapeCoords, shape[i]);
		}
		j = (*shape).n;
		float_arr_push (&shapeCoords, shape[j - 2]);
		float_arr_push (&shapeCoords, shape[j - 1]);
		float_arr_push (&shapeCoords, shape[0]);
		float_arr_push (&shapeCoords, shape[1]);
	}
	(*object)._coordinates = shapeCoords;
	return arr;
}
#endif

struct float_arr object_set_polyPoints (struct object *object,
		struct float_arr shape) {
	struct float_arr shapeCoords = float_arr_create (8);
	float_arr_push (&shapeCoords, shape.d[0]);
	float_arr_push (&shapeCoords, shape.d[1]);
	for(int i = 2; i < shape.n; i++){
		if (i % 2 == 0 && i > 2) {
			float_arr_push (&shapeCoords, shape.d[i - 2]);
			float_arr_push (&shapeCoords, shape.d[i - 1]);
		}
		float_arr_push (&shapeCoords, shape.d[i]);
	}
	size_t i = shape.n;
	float_arr_push (&shapeCoords, shape.d[i - 2]);
	float_arr_push (&shapeCoords, shape.d[i - 1]);
	float_arr_push (&shapeCoords, shape.d[0]);
	float_arr_push (&shapeCoords, shape.d[1]);
	// COME BACK. This originally used the getter/setter, if that's what it
	// is.
	(*object)._coordinates = shapeCoords;
	return shape;
}

struct float_arr object_set_coordinates (struct object *object,
		struct float_arr value) {
	(*object)._coordinates = value;
	(*object)._hasChanged = true;
	return value;
}

struct constraint_shape object_get_constraintShape (struct object *object) {
	return (*object)._constraintShape;
}

struct constraint_shape object_set_constraintShape (struct object *object,
		struct constraint_shape value) {
	(*object)._constraintShape = value;
	(*object)._hasChanged = true;
	return value;
}

bool object_get_hasChanged (struct object *object) {
	return (*object)._hasChanged;
}

bool object_set_hasChanged (struct object *object, bool value) {
	(*object)._hasChanged = value;
	return value;
}

struct edge_arr object_get_edges (struct object *object) {

	struct edge_arr res = edge_arr_create (8);
	struct constraint_segment_arr *seg = (*object)._constraintShape.segments;
	for (int i = 0; i < (*seg).n; i++) {
		for(int j = 0; j < (*(*seg).d[i].edges).n; j++)
			edge_arr_push (&res, (*(*seg).d[i].edges).d[j]);
	}

	return res;
}
