#pragma once
/* package hxDaedalus.data; */
#include "../data/math/triangle.h"

struct face;
struct vertex;
struct constraint_segment;

struct edge {
    int id; // get, never
    bool isReal; // (get, never)
    bool isConstrained; // (get, set)
    struct vertex *originVertex; // (get, set) 
    struct edge *nextLeftEdge; // (get, set)
    struct face *leftFace; // (get, set)
    struct constraint_segment_arr *fromConstraintSegments;
    struct vertex *destinationVertex; // (get, never)
    struct edge *oppositeEdge; // (get, never)
    struct edge *prevLeftEdge; // (get, never)
    struct edge *nextRightEdge; // (get, never)
    struct edge *prevRightEdge; // (get, never)
    struct edge *rotLeftEdge; // (get, never)
    struct edge *rotRightEdge; // (get, never)
    struct face *rightFace; // (get, never)
    struct triangle triangle; // (get, never)

	int INC;
	int _id;

	// root datas
	bool _isReal;
	bool _isConstrained;
	struct vertex *_originVertex;
	struct edge *_oppositeEdge;
	struct edge *_nextLeftEdge;
	struct face *_leftFace;

	int colorDebug;
};

DECLARE_ARR (struct edge, edge);

struct edge edge_new ();
int edge_get_id (struct edge *edge);
bool edge_get_isReal (struct edge *edge);
bool edge_get_isConstrained (struct edge *edge);
void edge_setDatas (struct edge *edge, struct vertex *originVertex,
		struct edge *oppositeEdge, struct edge *nextLeftEdge,
		struct face *leftFace, bool isReal, bool isConstrained);
void edge_addFromConstraintSegment (struct edge *edge,
		struct constraint_segment *segment);
void edge_removeFromConstraintSegment (struct edge *edge,
		struct constraint_segment *segment);
void edge_dispose (struct edge *edge);
struct vertex *edge_set_originVertex (struct edge *edge, struct vertex *value);
struct edge *edge_set_nextLeftEdge (struct edge *edge, struct edge *value);
struct face *edge_set_leftFace (struct edge *edge, struct face *value);
bool edge_set_isConstrained (struct edge *edge, bool value);
struct vertex *edge_get_originVertex(struct edge *edge);
struct vertex *edge_get_destinationVertex(struct edge *edge);
struct edge *edge_get_oppositeEdge (struct edge *edge);
struct edge *edge_get_nextLeftEdge (struct edge *edge);
struct edge *edge_get_prevLeftEdge (struct edge *edge);
struct edge *edge_get_nextRightEdge(struct edge *edge);
struct edge *edge_get_prevRightEdge(struct edge *edge);
struct edge *edge_get_rotLeftEdge(struct edge *edge);
struct edge *edge_get_rotRightEdge(struct edge *edge);
struct face *edge_get_leftFace (struct edge *edge);
struct face *edge_get_rightFace (struct edge *edge);
struct triangle edge_get_triangle (struct edge *edge);
char *edge_toString (struct edge *edge);
