#include "rand-generator.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

// NOTE: default seed was = 1234, default range_max was 1.
// So they're probably good values.
struct rand_generator rand_generator_new (int seed, int rangeMin,
		int rangeMax) {
	struct rand_generator res = {
		._originalSeed = seed,
		._currSeed = seed,
		.rangeMin = rangeMin,
		.rangeMax = rangeMax,
		._numIter = 0,
	};
	return res;
}

int rand_generator_set_seed (struct rand_generator *rand_generator,
		int value) {
	(*rand_generator)._originalSeed = (*rand_generator)._currSeed = value;
	return value;
}


int rand_generator_get_seed (struct rand_generator *rand_generator) {
	return (*rand_generator)._originalSeed;
}

void rand_generator_reset (struct rand_generator *rand_generator) {
	(*rand_generator)._currSeed = (*rand_generator)._originalSeed;
	(*rand_generator)._numIter = 0;
}

// COME BACK. I think I'm doing this right, but there's a good change I'm
// not.
// Think about integer division, too.
int rand_generator_next (struct rand_generator *rand_generator) {

	// They put this into the class. I think maybe you can only put things
	// in classes? Or maybe it just habit. Or maybe there's a good reason
	// to hang on to the string.

    static char _tempString[64]; // Probably a good length

	float _floatSeed = (*rand_generator)._currSeed;
	sprintf (_tempString, "%f", _floatSeed *
		 _floatSeed);
	int len;
	while ((len = strlen (_tempString)) < 8) {
		memmove (_tempString + 1, _tempString, len);
		_tempString[0] = '0';
	}

	_tempString[1 + 5] = '\0';
	(*rand_generator)._currSeed = strtol (_tempString, NULL, 10);
	int res = round ((*rand_generator).rangeMin + ((*rand_generator)._currSeed
				/ 99999) * ((*rand_generator).rangeMax -
					(*rand_generator).rangeMin));
	if ((*rand_generator)._currSeed == 0)
		(*rand_generator)._currSeed = (*rand_generator)._originalSeed +
			(*rand_generator)._numIter;
	(*rand_generator)._numIter++;
	if ((*rand_generator)._numIter == 200)
		rand_generator_reset (rand_generator);
	return res;
}

int rand_generator_nextInRange (struct rand_generator *rand_generator,
		int rangeMin, int rangeMax) {
	// COME BACK. This was this.rangeMin. An alternative to leaving it out?
	(*rand_generator).rangeMin = rangeMin;
	(*rand_generator).rangeMax = rangeMax;
	return rand_generator_next (rand_generator);
}

// Knuth shuffle
void rand_generator_shuffle (struct rand_generator *rand_generator,
		struct void_p_arr array) {
	size_t currIdx = array.n;

	while (currIdx > 0) {
		int rndIdx = rand_generator_nextInRange(rand_generator, 0,
				currIdx - 1);
		currIdx--;

		void *tmp = array.d[currIdx];
		array.d[currIdx] = array.d[rndIdx];
		array.d[rndIdx] = tmp;
	}
}

