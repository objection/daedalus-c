#include "triangle.h"

struct triangle triangle_new (struct vertex *va, struct vertex *vb,
		struct vertex *vc) {
	struct triangle res = {
		.a = va.pos,
		.b = vb.pos,
		.c = vc.pos,
	};
	return res;
}

bool triangle_is_solid (struct triangle *triangle) { // clockWise / antiClockwise used to decide to fill
	return (((*triangle).a.x * (*triangle).b.y - (*triangle).b.x *
			(*triangle).a.y) + ((*triangle).b.x * (*triangle).c.y -
			(*triangle).c.x * (*triangle).b.y) + ((*triangle).c.x *
			(*triangle).a.y - (*triangle).a.x * (*triangle).c.y))
		< 0;
}

// COME BACK. This was public static.
struct triangle triangle_fromFace (struct face face) {
	return face.edge.triangle;
}
