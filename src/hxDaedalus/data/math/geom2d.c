#include "geom2d.h"

struct vertex_arr __samples;
struct rand_generator _randGen;
bool rand_initted;

struct intersection geom2d_locatePosition (struct geom2d *geom2d, float x,
		float y, struct mesh mesh)
{
	//  COME BACK. __samples, I'm guessing is scratch space. So I'll make it
	//  local to the file and put in this if. It's not idea. I could put in
	//  a daedalus_init function.
	if (!(*geom2d).__samples.d) (*geom2d).__samples.d = create_vertex_arr (8);

	// jump and walk algorithm
	/* if ((*geom2d)._randGen == null) _randGen = new RandGenerator (); */
	// COME BACK. This is maybe dodgy. And the if could be bad. Again, maybe
	// an init function is the way.
	if (!rand_initted) {
		(*geom2d)._randGen = new RandGenerator ();
		rand_initted = 1;
	}
	// COME BACK. Doesthis work the way it should? Maybe casting rules are
	// different in haxe and C.
	(*geom2d)._randGen.seed = (int) (x * 10 + 4 * y);

	int i;

	// COME BACK. This, it seems is resetting __samples, which, it seems, is
	// a buffer that gets reused. It shouldn't even be part of the struct for a
	// start (I think). I'm just setting .n to 0, which should be fine,
	// shouldn't cause memory leaks. I've left the remove call commented out.
	/* vertex_arr_remove (&(*geom2d).__samples, 0, (*geom2d).__samples.n); */
	(*geom2d).__samples.n = 0;
	/* __samples.splice (0, __samples.length); */
	int numSamples = (int) (pow (mesh._vertices.n, 1 / 3));
	(*geom2d)._randGen.rangeMin = 0;
	(*geom2d)._randGen.rangeMax = mesh._vertices.length - 1;
	for (int i =0 ; i < numSamples; i++) {
		int _rnd = rand_generator (&(*geom2d)._randGen);

		Debug.assertFalse (_rnd < 0 || _rnd > mesh._vertices.length - 1, '_rnd: $_rnd');
		Debug.assertFalse (mesh._vertices == null, 'vertices: ${mesh._vertices.length}');
		__samples.push (mesh._vertices[_rnd]);
	}

	var currVertex : Vertex;
	var currVertexPos : Point2D;
	var distSquared : Float;
	var minDistSquared : Float = Math.POSITIVE_INFINITY;
	var closedVertex : Vertex = null;
	for (i in 0...numSamples){
		currVertex = __samples[i];
		currVertexPos = currVertex.pos;
		distSquared = (currVertexPos.x - x) * (currVertexPos.x - x) + (currVertexPos.y - y) * (currVertexPos.y - y);
		if (distSquared < minDistSquared)
		{
			minDistSquared = distSquared;
			closedVertex = currVertex;
		}
	}

	var currFace : Face;
	var iterFace : FromVertexToHoldingFaces = new FromVertexToHoldingFaces ();
	iterFace.fromVertex = closedVertex;
	currFace = iterFace.next ();

	var faceVisited = new Map<Face,Bool>();
	var currEdge : Edge;
	var iterEdge : FromFaceToInnerEdges = new FromFaceToInnerEdges ();
	var objectContainer : Intersection = ENull;
	var relativPos : Int;
	var numIter : Int = 0;
	//while ( faceVisited[ currFace ] || !(objectContainer = isInFace (x, y, currFace)) )
	while ( faceVisited[ currFace ] || (objectContainer = isInFace (x, y, currFace)).match (ENull) )
	{
		faceVisited[ currFace ];

		numIter++;
		if (numIter == 50)
		{
			Debug.trace ("WALK TOOK MORE THAN 50 LOOPS");
		}
		iterEdge.fromFace = currFace;
		do
		{
			currEdge = iterEdge.next ();
			if (currEdge == null)
			{
				Debug.trace ("KILL PATH");
				return ENull;
			}
			relativPos = getRelativePosition (x, y, currEdge);
		} while ((relativPos == 1 || relativPos == 0));

		currFace = currEdge.rightFace;
	}

	return objectContainer;
}

bool geom2d_isCircleIntersectingAnyConstraint (float x, float y,
		float radius, struct mesh mesh) {
	if (x <= 0 || x >= mesh.width || y <= 0 || y >= mesh.height)
		return true;

	var loc = Geom2D.locatePosition(x, y, mesh);
	var face : Face;
	switch( loc ){
		case EVertex( vertex ):
			face = vertex.edge.leftFace;
		case EEdge( edge ):
			face = edge.leftFace;
		case EFace( face_ ):
			face = face_;
		case ENull:
			face = null;
	}

	// if a vertex is in the circle, a contrainst must intersect the circle
	// because a vertex always belongs to a contrained edge
	var radiusSquared : Float = radius * radius;
	var pos : Point2D;
	var distSquared : Float;
	pos = face.edge.originVertex.pos;
	distSquared = (pos.x - x) * (pos.x - x) + (pos.y - y) * (pos.y - y);
	if (distSquared <= radiusSquared)
	{
		return true;
	}
	pos = face.edge.nextLeftEdge.originVertex.pos;
	distSquared = (pos.x - x) * (pos.x - x) + (pos.y - y) * (pos.y - y);
	if (distSquared <= radiusSquared)
	{
		return true;
	}
	pos = face.edge.nextLeftEdge.nextLeftEdge.originVertex.pos;
	distSquared = (pos.x - x) * (pos.x - x) + (pos.y - y) * (pos.y - y);
	if (distSquared <= radiusSquared)
	{
		return true;
	}  // check if edge intersects



	var edgesToCheck = new Array<Edge>();
	edgesToCheck.push(face.edge);
	edgesToCheck.push(face.edge.nextLeftEdge);
	edgesToCheck.push(face.edge.nextLeftEdge.nextLeftEdge);

	var edge : Edge;
	var pos1 : Point2D;
	var pos2 : Point2D;
	var checkedEdges = new Map<Edge,Bool>();
	var intersecting : Bool;
	while (edgesToCheck.length > 0)
	{
		edge = edgesToCheck.pop();
		checkedEdges[ edge ] = true;
		pos1 = edge.originVertex.pos;
		pos2 = edge.destinationVertex.pos;
		intersecting = intersectionsSegmentCircle(pos1.x, pos1.y, pos2.x, pos2.y, x, y, radius);
		if (intersecting)
		{
			if (edge.isConstrained) {
				return true;
			}else {
				edge = edge.oppositeEdge.nextLeftEdge;
				if (!checkedEdges[edge] && !checkedEdges[edge.oppositeEdge] && edgesToCheck.indexOf(edge) == -1 && edgesToCheck.indexOf(edge.oppositeEdge) == -1)
				{
					edgesToCheck.push(edge);
				}
				edge = edge.nextLeftEdge;
				if (!checkedEdges[edge] && !checkedEdges[edge.oppositeEdge] && edgesToCheck.indexOf(edge) == -1 && edgesToCheck.indexOf(edge.oppositeEdge) == -1)
				{
					edgesToCheck.push(edge);
				}
			}
		}
	}

	return false;
}

// squared distance from point p to infinite line (a, b)
float distanceSquaredPointToLine(float px, float py, float ax, float ay,
		float bx, float by) {
	float a_b_squaredLength = (bx - ax) * (bx - ax) + (by - ay) * (by - ay);
	float dotProduct = (px - ax) * (bx - ax) + (py - ay) * (by - ay);
	float p_a_squaredLength  = (ax - px) * (ax - px) + (ay - py) * (ay - py);
	return p_a_squaredLength - dotProduct * dotProduct / a_b_squaredLength;
}
