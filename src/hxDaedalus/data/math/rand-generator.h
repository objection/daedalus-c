#pragma once
#include "../../../global.h"

struct rand_generator {
    int seed; // (get, set)
    int rangeMin;
    int rangeMax;

     int _originalSeed;
     int _currSeed;
     int _rangeMin;
     int _rangeMax;
    
     int _numIter;
	
};
struct rand_generator rand_generator_new (int seed, int rangeMin,
		int rangeMax);
int rand_generator_set_seed (struct rand_generator *rand_generator,
		int value);
int rand_generator_get_seed (struct rand_generator *rand_generator);
void rand_generator_reset (struct rand_generator *rand_generator);
int rand_generator_next (struct rand_generator *rand_generator);
int rand_generator_nextInRange (struct rand_generator *rand_generator,
		int rangeMin, int rangeMax);
void rand_generator_shuffle (struct rand_generator *rand_generator,
		struct void_p_arr array);
