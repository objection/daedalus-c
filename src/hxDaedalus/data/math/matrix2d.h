#pragma once
/* package hxDaedalus.data.math; */

struct point2d;

struct matrix2d {
    float a;
    float b;
    float c;
    float d;
    float e;
    float f;

    /*
    hxDaedalusPoint2D represents row vector in homogeneous coordinates:
    [x, y, 1]

    hxDaedalusMatrix2D represents transform matrix in homogeneous coordinates:
    [a, b, 0]
    [c, d, 0]
    [e, f, 1]
    */
};

struct matrix2d matrix2d_new (float a, float b, float c, float d, float e,
		float f);
void matrix2d_identity (struct matrix2d *matrix2d);
void matrix2d_translate (struct matrix2d *matrix2d, float tx, float ty);
void matrix2d_scale (struct matrix2d *matrix2d, float sx, float sy);
void matrix2d_rotate(struct matrix2d *matrix2d, float rad);
struct matrix2d matrix2d_clone(struct matrix2d *matrix2d);
void matrix2d_transform (struct matrix2d *matrix2d, struct point2d *point);
float matrix2d_transformX (struct matrix2d *matrix2d, float x, float y);
float matrix2d_transformY (struct matrix2d *matrix2d, float x, float y);
void matrix2d_concat(struct matrix2d *matrix2d, struct matrix2d with);
