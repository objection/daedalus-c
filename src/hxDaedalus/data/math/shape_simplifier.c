#include "shape_simplifier.h"
#include "../../debug/debug.h"
#include "geom2d.h"

struct float_arr shape_simplifier_simplify (struct float_arr coords,
		float epsilon /* NOTE: epsilon default is 1 */ )  {
	struct float_arr res = float_arr_create (4);
	size_t len = coords.n;
	debug_assertFalse ((len & 1) != 0, "Wrong size");

	if (len <= 4 || epsilon < 1) {
		float_arr_insert (&res, coords.d, 0, coords.n);
		/* return [].concat (coords); */
	}

	float firstPointX = coords.d[0];
	float firstPointY = coords.d[1];
	float lastPointX = coords.d[coords.n - 2];
	float lastPointY = coords.d[coords.n - 1];

	int index = -1;
	float dist = 0;
	for (int i = 1; i < len >> 1; i++) {
		float currDist = geom2d_distanceSquaredPointToSegment (coords.d[i <<
				1], coords.d[(i << 1) + 1], firstPointX, firstPointY,
				lastPointX, lastPointY);
		if (currDist > dist) {
			dist = currDist;
			index = i;
		}
	}

	if (dist > epsilon * epsilon) {
		// recurse
		var l1 = coords.slice (0, (index << 1) + 2);
		var l2 = coords.slice (index << 1);
		var r1 = simplify (l1, epsilon);
		var r2 = simplify (l2, epsilon);
		// concat r2 to r1 minus the end/startpoint that will be the same
		var rs = r1.slice (0, r1.length - 2).concat (r2);
		return rs;
	} else {
		return [firstPointX, firstPointY, lastPointX, lastPointY];
	}
}

