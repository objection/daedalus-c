#pragma once
/* package hxDaedalus.data.math; */

struct matrix2d;

struct point2d {
#if 0 // COME BACK
    public var length(get, never) : Float;
#endif
    float x;
    float y;
};

struct point2d point2d_new (float x, float y);
void point2d_transform (struct point2d *point2d, struct matrix2d matrix);
void setXY (struct point2d *point2d, float x, float y);
struct point2d clone (struct point2d *point2d);
void subtract(struct point2d *point2d, struct point2d b);
float point2d_get_length (struct point2d *point2d);
void point2d_normalize (struct point2d *point2d);
void point2d_scale(struct point2d *point2d, float s);
float point2d_distanceTo (struct point2d *point2d, struct point2d to);
float distanceSquaredTo (struct point2d *point2d, struct point2d to);
