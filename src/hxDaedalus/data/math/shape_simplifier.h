#pragma once

#include "../../../global.h"
#include "../../debug/debug.h"

#if 0
struct shape_simplifier {

};
#endif
	/**
	 * Simplify polyline (Ramer-Douglas-Peucker).
	 *
	 * @param	coords		Array of coords defining the polyline.
	 * @param	epsilon		Perpendicular distance threshold (typically in the range [1..2]).
	 * @return	An array of coords defining the simplified polyline.
	 */
struct float_arr shape_simplifier_simplify (struct float_arr coords,
		float epsilon /* NOTE: epsilon default is 1 */ ); 
