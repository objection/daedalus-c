#include "point2d.h"
#include "matrix2d.h"
#include "math.h"

struct point2d point2d_new (float x, float y){
	struct point2d res = {
		.x = x,
		.y = y,
	};
	return res;
}

// COME BACK. Does/should matrix.transform take a pointer?
void point2d_transform (struct point2d *point2d, struct matrix2d matrix) {
	matrix2d_transform (&matrix, point2d);
}

void setXY (struct point2d *point2d, float x, float y) {
	(*point2d).x = x;
	(*point2d).y = y;
}

struct point2d clone (struct point2d *point2d) {
	return point2d_new ((*point2d).x, (*point2d).y);
}

void subtract(struct point2d *point2d, struct point2d b) {
	(*point2d).x -= b.x;
	(*point2d).y -= b.y;
}

float point2d_get_length (struct point2d *point2d) {
	return sqrt ((*point2d).x * (*point2d).x + (*point2d).y * (*point2d).y);
}

void point2d_normalize (struct point2d *point2d) {
	float norm = point2d_get_length (point2d);
	(*point2d).x = (*point2d).x / norm;
	(*point2d).y = (*point2d).y / norm;
}


void point2d_scale(struct point2d *point2d, float s) {
	(*point2d).x = (*point2d).x * s;
	(*point2d).y = (*point2d).y * s;
}

float point2d_distanceTo (struct point2d *point2d, struct point2d to) {
	float diffX = (*point2d).x - to.x;
	float diffY = (*point2d).y - to.y;
	return sqrt (diffX * diffX + diffY * diffY);
}

float distanceSquaredTo (struct point2d *point2d, struct point2d to) {
	float diffX = (*point2d).x - to.x;
	float diffY = (*point2d).y - to.y;
	return diffX * diffX + diffY * diffY;
}
