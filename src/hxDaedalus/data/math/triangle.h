#pragma once

#include "../vertex.h"
#include "../face.h"
#include "point2d.h"

struct triangle {
    struct point2d a;
    struct point2d b;
    struct point2d c;
};
struct triangle triangle_new (struct vertex *va, struct vertex *vb,
		struct vertex *vc);
bool triangle_is_solid (struct triangle *triangle);
struct triangle triangle_fromFace (struct face face);
