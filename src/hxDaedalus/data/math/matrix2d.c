#include "matrix2d.h"
#include "point2d.h"
#include "math.h"

struct matrix2d matrix2d_new (float a, float b, float c, float d, float e,
		float f) {
	struct matrix2d res = {
		.a = a,
		.b = b,
		.c = c,
		.d = d,
		.e = e,
		.f = f,
	};
	return res;
}

// NOTE: was public
void matrix2d_identity (struct matrix2d *matrix2d) {
	/*
	   [1, 0, 0]
	   [0, 1, 0]
	   [0, 0, 1]
	   */

	(*matrix2d).a = 1;
	(*matrix2d).b = 0;
	(*matrix2d).c = 0;
	(*matrix2d).d = 1;
	(*matrix2d).e = 0;
	(*matrix2d).f = 0;
}

void matrix2d_translate (struct matrix2d *matrix2d, float tx, float ty) {
	/*
	   [1,  0,  0]
	   [0,  1,  0]
	   [tx, ty, 1]

*/
	(*matrix2d).e = (*matrix2d).e + tx;
	(*matrix2d).f = (*matrix2d).f + ty;
}


void matrix2d_scale (struct matrix2d *matrix2d, float sx, float sy) {
	/*
	   [sx, 0, 0]
	   [0, sy, 0]
	   [0,  0, 1]
	   */
	(*matrix2d).a = (*matrix2d).a * sx;
	(*matrix2d).b = (*matrix2d).b * sy;
	(*matrix2d).c = (*matrix2d).c * sx;
	(*matrix2d).d = (*matrix2d).d * sy;
	(*matrix2d).e = (*matrix2d).e * sx;
	(*matrix2d).f = (*matrix2d).f * sy;
}

void matrix2d_rotate(struct matrix2d *matrix2d, float rad) {
	/*
	   [ cos, sin, 0]
	   [-sin, cos, 0]
	   [   0,   0, 1]

	   [a, b, 0]
	   [c, d, 0]
	   [e, f, 1]
	   */
	float _cos = cos (rad);
	float _sin = sin (rad);
	float a = (*matrix2d).a * _cos + (*matrix2d).b * -_sin;
	float b = (*matrix2d).a * _sin + (*matrix2d).b * _cos;
	float c = (*matrix2d).c * _cos + (*matrix2d).d * -_sin;
	float d = (*matrix2d).c * _sin + (*matrix2d).d * _cos;
	float e = (*matrix2d).e * _cos + (*matrix2d).f * -_sin;
	float f = (*matrix2d).e * _sin + (*matrix2d).f * _cos;
	(*matrix2d).a = a;
	(*matrix2d).b = b;
	(*matrix2d).c = c;
	(*matrix2d).d = d;
	(*matrix2d).e = e;
	(*matrix2d).f = f;
}

struct matrix2d matrix2d_clone(struct matrix2d *matrix2d) {
	return matrix2d_new ((*matrix2d).a, (*matrix2d).b, (*matrix2d).c,
			(*matrix2d).d, (*matrix2d).e, (*matrix2d).f);
}

// COME BACK. This function just assigns point, and ends, so I assume it's
// meant to be a pointer?
//
// NOtE: this was called "matrix2d_tranform", which I presume is a spelling
// mistake.
void matrix2d_transform (struct matrix2d *matrix2d, struct point2d *point) {
	/*
	   [a, b, 0]
	   [c, d, 0]
	   [e, f, 1]
	   [x, y, 1]
	   */
	float x = (*matrix2d).a * (*point).x + (*matrix2d).c * (*point).y +
		(*matrix2d).e;
	float y = (*matrix2d).b * (*point).x + (*matrix2d).d * (*point).y +
		(*matrix2d).f;
	(*point).x = x;
	(*point).y = y;
}

float matrix2d_transformX (struct matrix2d *matrix2d, float x, float y) {
	return (*matrix2d).a * x + (*matrix2d).c * y + (*matrix2d).e;
}

float matrix2d_transformY (struct matrix2d *matrix2d, float x, float y) {
	return (*matrix2d).b * x + (*matrix2d).d * y + (*matrix2d).f;
}

void matrix2d_concat(struct matrix2d *matrix2d, struct matrix2d with) {
	float a = (*matrix2d).a * with.a + (*matrix2d).b * with.c;
	float b = (*matrix2d).a * with.b + (*matrix2d).b * with.d;
	float c = (*matrix2d).c * with.a + (*matrix2d).d * with.c;
	float d = (*matrix2d).c * with.b + (*matrix2d).d * with.d;
	float e = (*matrix2d).e * with.a + (*matrix2d).f * with.c + with.e;
	float f = (*matrix2d).e * with.b + (*matrix2d).f * with.d + with.f;
	(*matrix2d).a = a;
	(*matrix2d).b = b;
	(*matrix2d).c = c;
	(*matrix2d).d = d;
	(*matrix2d).e = e;
	(*matrix2d).f = f;
}
