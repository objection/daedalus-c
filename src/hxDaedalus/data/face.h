#pragma once
#include "../../global.h"
/* package hxDaedalus.data; */

/* struct edge; */

struct face {
	int id; // (get, never) : Int;
	bool isReal; //  (get, never)
    struct edge *edge; // (get, never)
	struct edge *_edge;

	// This is declared static in the haxe code. It seems like that's the
	// same as static in a C function, except it's local to the class.
	//
	// I'm simlulating this by defining INC in face.c and setting it from
	// that in face_new.
	int INC;
	int _id;

	bool _isReal;

	// NOTE: public in original
	int colorDebug;
};

DECLARE_ARR (struct face, face);

struct face face_new (int id);
int face_get_id (struct face *face);
bool face_get_isReal (struct face *face);
void face_set_datas (struct face *face, struct edge *edge);
// must use this if you want te set isReal to false.
// NOTE: public in the original.
void face_setDatas (struct face *face, struct edge *edge, bool isReal);
void face_dispose (struct face *face);
struct edge *face_get_edge (struct face *face);
