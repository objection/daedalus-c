#pragma once
/* package hxDaedalus.data; */
#include "../data/math/point2d.h"
#include "../data/edge.h"
#include "constraint-segment.h"

struct vertex {
	int id; //  (get, never) 
	bool isReal; // (get, never)
	struct point2d pos; // (get, never)
	struct constraint_segment_arr *fromConstraintSegments; // (get, set)
	struct edge *edge; // (get, set)

	int _id;

	struct point2d  _pos;

	bool _isReal;
	struct edge *_edge;

	struct constraint_segment_arr *_fromConstraintSegments;

	// NOTE: was public
	int colorDebug;


};

DECLARE_ARR (struct vertex, vertex);
struct vertex vertex_new ();
int vertex_get_id (struct vertex *vertex);
bool vertex_get_isReal (struct vertex *vertex);
struct point2d vertex_get_pos (struct vertex *vertex);
struct constraint_segment_arr *vertex_get_fromConstraintSegments (struct vertex
		*vertex);
struct constraint_segment_arr *vertex_set_fromConstraintSegments (struct vertex
		*vertex, struct constraint_segment_arr *value);
void vertex_setDatas (struct vertex *vertex, struct edge *edge, bool isReal);
void vertex_addFromConstraintSegment (struct vertex *vertex,
		struct constraint_segment *segment);
void vertex_removeFromConstraintSegment (struct vertex *vertex,
		struct constraint_segment *segment);
void vertex_dispose (struct vertex *vertex);
struct edge *vertex_get_edge (struct vertex *vertex);
struct edge *set_edge (struct vertex *vertex, struct edge *value);
char *toString (struct vertex *vertex);
