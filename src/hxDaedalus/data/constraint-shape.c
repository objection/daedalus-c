#include "constraint-shape.h"

DEFINE_ARR (struct constraint_shape, constraint_shape);
int INC;

struct constraint_shape constraint_shape_new () {

	struct constraint_shape res = {
		._id = INC++,
		// Has to be a pointer.
		.segments = malloc (sizeof *res.segments),
	};
	*res.segments = constraint_segment_arr_create (8);
	return res;
}

int constraint_shape_get_id (struct constraint_shape *constraint_shape) {
	return (*constraint_shape)._id;
}

void constraint_shape_dispose (struct constraint_shape *shape) {
	while ((*(*shape).segments).n > 0) {
		constraint_segment_dispose
			(&(*(*shape).segments).d[(*(*shape).segments).n]);
		constraint_segment_arr_pop ((*shape).segments, 1);
	}
}
