

#define EPSILON = 0.01;
#define EPSILON_SQUARED = 0.0001;
#if 0
class constants {
    public static inline var EPSILON = 0.01;
    public static inline var EPSILON_SQUARED = 0.0001;
}
/*
@:enum
abstract Constants( Float ) {
    var EPSILON = 0.01;
    var EPSILON_SQUARED = 0.0001;
}
*/
#endif
