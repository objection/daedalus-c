#pragma once
/* package hxDaedalus.iterators; */
#include "../data/edge.h"
#include "../data/face.h"

struct from_face_to_inner_edges {
    /* public var fromFace( never, set ): Face; */
    struct face _fromFace;
    struct edge _nextEdge;
    struct edge _resultEdge;
};

struct from_face_to_inner_edges from_face_to_inner_edges_new (); 
struct face from_face_to_inner_edges_set_fromFace (struct face *face, struct face value);
struct edge from_face_to_inner_edges_edge_next (struct face *face);
