#pragma once
/* package hxDaedalus.iterators; */
#include "../data/edge.h"
#include "../data/face.h"

struct from_face_to_neighbour_faces {
	// COME BACK. I assume these should all be pointers. Even fromFace?
    struct face *fromFace; // (never, set)
    struct face *_fromFace;
    struct edge *_nextEdge;
    struct face *_resultFace;

};

struct from_face_to_neighbour_faces from_face_to_neighbour_faces_new ();
struct face from_face_to_neighbour_faces_set_fromFace (
		struct from_face_to_neighbour_faces_set_fromFace *fftnf,
		struct face value);
struct face from_face_to_neighbour_faces_next (struct
		from_face_to_neighbour_faces_set_fromFace *fftnf);
