#include "from-face-to-neighbour-faces.h"

struct from_face_to_neighbour_faces from_face_to_neighbour_faces_new (){
	fprintf (stderr, "Should probably take this out since it just returns the \
struct");
	return (struct from_face_to_neighbour_faces) {0};
}

struct face from_face_to_neighbour_faces_set_fromFace (
		struct from_face_to_neighbour_faces_set_fromFace *fftnf,
		struct face value) {
	(*fftnf)._fromFace = value;
	(*fftnf)._nextEdge = (*fftnf)._fromFace.edge;
	return value;
}

struct face from_face_to_neighbour_faces_next (struct
		from_face_to_neighbour_faces_set_fromFace *fftnf) {
	if ((*fftnf)._nextEdge != NULL) {
		do {
			(*fftnf)._resultFace = (*fftnf)._nextEdge.rightFace;
			(*fftnf)._nextEdge = (*fftnf)._nextEdge.nextLeftEdge;
			if ((*fftnf)._nextEdge == (*fftnf)._fromFace.edge) {
				(*fftnf)._nextEdge = NULL;
				if (!(*fftnf)._resultFace.isReal) (*fftnf)._resultFace = NULL;
				break;
			}
		} while ((!(*fftnf)._resultFace.isReal));
	} else
		(*fftnf)._resultFace = NULL;
	return (*fftnf)._resultFace;
}
