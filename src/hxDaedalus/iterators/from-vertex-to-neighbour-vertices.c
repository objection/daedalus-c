#include "from-vertex-to-neighbour-vertices.h"

// COME BACK
/* public function new(){} */

struct vertex *from_vertex_to_neighbour_vertices_set_fromVertex (
		struct from_vertex_to_neighbour_vertices *fvtnv,
		struct vertex *value) {
	(*fvtnv)._fromVertex = value;
	(*fvtnv)._nextEdge = (*(*fvtnv)._fromVertex).edge;
	return value;
}

struct vertex *from_vertex_to_neighbour_vertices_next (
		struct from_vertex_to_neighbour_vertices *fvtnv) {
	if ((*fvtnv)._nextEdge != NULL) {
		(*fvtnv)._resultVertex =
			(*(*(*fvtnv)._nextEdge).oppositeEdge).originVertex;
		do {
			(*fvtnv)._nextEdge = (*(*fvtnv)._nextEdge).rotLeftEdge;
		} while ((!(*(*fvtnv)._nextEdge).isReal));

		if ((*fvtnv)._nextEdge == (*(*fvtnv)._fromVertex).edge)
			(*fvtnv)._nextEdge = NULL;
	} else
		(*fvtnv)._resultVertex = NULL;

	return (*fvtnv)._resultVertex;
}
