#include "from-face-to-inner-vertices.h"

struct from_face_to_inner_vertices from_face_to_inner_vertices_new () {
	return (struct from_face_to_inner_vertices) {0};
}

struct face *from_face_to_inner_vertices_set_fromFace (
		struct from_face_to_inner_vertices *fftiv, struct face *value) {
	(*fftiv)._fromFace = value;
	(*fftiv)._nextEdge = (*(*fftiv)._fromFace)._edge;
	return value;
}

struct vertex *from_face_to_inner_vertices_next (
		struct from_face_to_inner_vertices *fftiv) {
	if ((*fftiv)._nextEdge != NULL ) {
		(*fftiv)._resultVertex = (*(*fftiv)._nextEdge).originVertex;
		(*fftiv)._nextEdge = (*(*fftiv)._nextEdge).nextLeftEdge;
		if ((*fftiv)._nextEdge == (*(*fftiv)._fromFace).edge )
			(*fftiv)._nextEdge = NULL;
	} else {
		(*fftiv)._resultVertex = NULL;
	}
	return (*fftiv)._resultVertex;
}
