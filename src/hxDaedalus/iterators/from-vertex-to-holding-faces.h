#pragma once
#include "../data/edge.h"
#include "../data/face.h"
#include "../data/vertex.h"

struct from_vertex_to_holding_faces {
    struct vertex *fromVertex; // (never, set)
    struct vertex *_fromVertex;
    struct edge *_nextEdge;
    struct face *_resultFace;
};
