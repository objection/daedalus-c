/* package hxDaedalus.iterators; */
#include "../data/edge.h"
#include "../data/vertex.h"

struct from_vertex_to_neighbour_vertices {
    struct vertex *fromVertex; // ( never, set )
    struct vertex *_fromVertex;
    struct edge *_nextEdge;
    struct vertex *_resultVertex;

};
struct vertex *from_vertex_to_neighbour_vertices_set_fromVertex (
		struct from_vertex_to_neighbour_vertices *fvtnv,
		struct vertex *value);
struct vertex *from_vertex_to_neighbour_vertices_next (
		struct from_vertex_to_neighbour_vertices *fvtnv);
