#include "from-vertex-to-incoming-edges.h"

struct vertex *from_vertex_to_incoming_edges_set_fromVertex (
		struct from_vertex_to_incoming_edges *fvtie, struct vertex
		*value) {
	(*fvtie)._fromVertex = value;
	(*fvtie)._nextEdge = (*(*fvtie)._fromVertex).edge;
	while (!(*(*fvtie)._nextEdge).isReal)
		(*fvtie)._nextEdge = (*(*fvtie)._nextEdge).rotLeftEdge;
	return value;
}

struct edge *from_vertex_to_incoming_edges_next (
		struct from_vertex_to_incoming_edges *fvtie) {
	if ((*fvtie)._nextEdge != NULL) {
		(*fvtie)._resultEdge = (*(*fvtie)._nextEdge).oppositeEdge;
		do {
			(*fvtie)._nextEdge = (*(*fvtie)._nextEdge).rotLeftEdge;
			if ((*fvtie)._nextEdge == (*(*fvtie)._fromVertex).edge) {
				(*fvtie)._nextEdge = NULL;
				break;
			}
		} while ((!(*(*fvtie)._nextEdge).isReal));
	} else
		(*fvtie)._resultEdge = NULL;
	return (*fvtie)._resultEdge;
}
