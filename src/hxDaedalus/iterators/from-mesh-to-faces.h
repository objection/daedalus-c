#pragma once

#include "../data/face.h"
#include "../data/mesh.h"
#include "../data/vertex.h"

struct from_mesh_to_faces {
    struct mesh *fromMesh; // (never, set)
    struct mesh *_fromMesh
    int _currIndex;
    struct face *_resultFace;
}
