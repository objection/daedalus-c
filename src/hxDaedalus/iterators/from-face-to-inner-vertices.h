/* package hxDaedalus.iterators; */
#include "../data/edge.h"
#include "../data/face.h"
#include "../data/vertex.h"

struct from_face_to_inner_vertices {
    /* public var fromFace( never, set ): Face; */
    struct face *_fromFace;
    struct edge *_nextEdge;
    struct vertex *_resultVertex;
};
struct from_face_to_inner_vertices from_face_to_inner_vertices_new ();
struct face *from_face_to_inner_vertices_set_fromFace (
		struct from_face_to_inner_vertices *fftiv, struct face *value);
struct vertex *from_face_to_inner_vertices_next (
		struct from_face_to_inner_vertices *fftiv);
