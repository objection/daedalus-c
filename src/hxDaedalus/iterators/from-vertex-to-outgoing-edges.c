#include "from-vertex-to-outgoing-edges.h"

struct from_vertex_to_outgoing_edges from_vertex_to_outgoing_edges_new () {
	return (struct from_vertex_to_outgoing_edges) {
		.realEdgesOnly = 1,
	};
}

struct vertex *from_vertex_to_outgoing_edges_set_fromVertex (struct
		from_vertex_to_outgoing_edges *fvtoe, struct vertex *value) {
	(*fvtoe)._fromVertex = value;
	(*fvtoe)._nextEdge = (*(*fvtoe)._fromVertex).edge;
	while ((*fvtoe).realEdgesOnly && !(*(*fvtoe)._nextEdge).isReal) {
		(*fvtoe)._nextEdge = (*(*fvtoe)._nextEdge).rotLeftEdge;
	}
	return value;
}

struct edge *from_vertex_to_outgoing_edges_next (struct from_vertex_to_outgoing_edges *fvtoe) {
	if ((*fvtoe)._nextEdge != NULL) {
		(*fvtoe)._resultEdge = (*fvtoe)._nextEdge;
		do {
			(*fvtoe)._nextEdge = (*(*fvtoe)._nextEdge).rotLeftEdge;
			if ((*fvtoe)._nextEdge == (*(*fvtoe)._fromVertex).edge) {
				(*fvtoe)._nextEdge = NULL;
				break;
			}
		} while (((*fvtoe).realEdgesOnly && !(*(*fvtoe)._nextEdge).isReal));
	}
	else (*fvtoe)._resultEdge = NULL;

	return (*fvtoe)._resultEdge;
}
