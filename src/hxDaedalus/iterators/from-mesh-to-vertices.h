#pragma once
/* package hxDaedalus.iterators; */
#include "../data/face.h"
#include "../data/mesh.h"
#include "../data/vertex.h"

struct from_mesh_to_vertices {
    /* public var fromMesh(never, set) : Mesh; */
	// COME BACK. Should be pointer?
    struct mesh *_fromMesh;
    int _currIndex;
    struct vertex *_resultVertex;
};
