#pragma once
/* package hxDaedalus.iterators; */

#include "../data/edge.h"
#include "../data/vertex.h"

struct from_vertex_to_outgoing_edges {
	struct vertex *fromVertex; // (never, set)


	struct vertex *_fromVertex;
	struct edge *_nextEdge;

	bool realEdgesOnly; // = true
	struct edge *_resultEdge;

};

struct from_vertex_to_outgoing_edges from_vertex_to_outgoing_edges_new ();
struct vertex *from_vertex_to_outgoing_edges_set_fromVertex (
		struct from_vertex_to_outgoing_edges *fvtoe, struct vertex *value);
struct edge *from_vertex_to_outgoing_edges_next (struct
		from_vertex_to_outgoing_edges *fvtoe);
