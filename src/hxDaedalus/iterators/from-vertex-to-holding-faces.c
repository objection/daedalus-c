#include "from-vertex-to-holding-faces.h"

// COME BACK. Not putting this in. Might need to.
// public function new(){}

struct vertex *from_vertex_to_holding_faces_set_fromVertex (
		struct from_vertex_to_holding_faces *fvthf,
		struct vertex *value) {
	(*fvthf)._fromVertex = value;
	(*fvthf)._nextEdge = (*(*fvthf)._fromVertex).edge;
	return value;
}

struct face *from_vertex_to_holding_faces_next (
		struct from_vertex_to_holding_faces *fvthf) {
	if ((*fvthf)._nextEdge != NULL) {
		do {
			(*fvthf)._resultFace = (*(*fvthf)._nextEdge).leftFace;
			(*fvthf)._nextEdge = (*(*fvthf)._nextEdge).rotLeftEdge;
			if ((*fvthf)._nextEdge == (*(*fvthf)._fromVertex).edge) {
				(*fvthf)._nextEdge = NULL;
				if (!(*(*fvthf)._resultFace).isReal)
					(*fvthf)._resultFace = NULL;
				break;
			}
		} while (!(*(*fvthf)._resultFace).isReal);
	} else {
		(*fvthf)._resultFace = NULL;
	}
	return (*fvthf)._resultFace;
}
