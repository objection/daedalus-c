#include "from-mesh-to-vertices.h"

// COME BACK
/* struct from_mesh_to_vertices from_mesh_to_vertices_new() {} */

struct mesg *set_fromMesh (struct from_mesh_to_vertices *fmtv,
		struct mesh value) {
	(*fmtv)._fromMesh = *value;
	(*fmtv)._currIndex = 0;
	return value;
}

struct vertex from_mesh_to_vertices_next (struct from_mesh_to_vertices *fmtv) {
	do {
		if ((*fmtv)._currIndex < (*fmtv)._fromMesh._vertices.length ){
			(*fmtv)._resultVertex =
				(*fmtv)._fromMesh._vertices[(*fmtv)._currIndex ];
			(*fmtv)._currIndex++;
		} else {
			(*fmtv)._resultVertex = NULL;
			break;
		}
	} while ((!(*fmtv)._resultVertex.isReal));
	return (*fmtv)._resultVertex;
}
