#include "from-face-to-inner-edges.h"

// NOTE: In the original this was an empty constructor. I'll still write this
// function.
struct from_face_to_inner_edges from_face_to_inner_edges_new () {
	return (struct from_face_to_inner_edges) {0};
}

// NOTE: this is insanely wordy. But I should stick to their stucker. "Just
// abbreviate it." Yeah, but you forget what the abbreviation means.
struct face from_face_to_inner_edges_face_set_fromFace (struct
		from_face_to_inner_edges *fftie, struct face value) {
	(*fftie)._fromFace = value;
	(*fftie)._nextEdge = (*fftie)._fromFace.edge;
	return value;
}

// COME BACK. This does nulls. Maybe I should write an optional type. It
// probably wont be simpler.
struct edge from_face_to_inner_edges_edge_next (struct face *face) {
	if ((*face)._nextEdge != null) {
		(*face)._resultEdge = (*face)._nextEdge;
		(*face)._nextEdge = (*face)._nextEdge.nextLeftEdge;
		if ((*face)._nextEdge == (*face)._fromFace.edge)
			(*face)._nextEdge = null;
	} else
		(*face)._resultEdge = null;
	return _resultEdge;
}
