/* package hxDaedalus.iterators; */
#include "../data/edge.h"
#include "../data/vertex.h"

struct from_vertex_to_incoming_edges {
    struct vertex *fromVertex; // (never, set)
    struct vertex *_fromVertex;
    struct edge *_nextEdge;
    struct edge *_resultEdge;
};
struct vertex *from_vertex_to_incoming_edges_set_fromVertex (
		struct from_vertex_to_incoming_edges *fvtie, struct vertex
		*value);
struct edge *from_vertex_to_incoming_edges_next (
		struct from_vertex_to_incoming_edges *fvtie);
