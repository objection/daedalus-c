/* package hxDaedalus.debug; */

/* #include haxe.PosInfos; */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * Used for sanity-checks throughout the code when in debug mode (or if -D DAEDALUS_CHECKS is passed to the compiler).
 * Should be automatically stripped out by the compiler in release mode (or if -D NO_DAEDALUS_CHECKS is passed to the compiler).
 * Same applies to -D DAEDALUS_TRACE and -D NO_DAEDALUS_TRACE.
 */

/* void _debug_assertTrue (bool cond, char *message, char *file, int line); */
/* void _debug_assertFalse (bool cond, char *message, char *file, int line); */
#if ((debug && !NO_DAEDALUS_CHECKS) || DAEDALUS_CHECKS)

void _debug_assertTrue (bool cond, char *message, char *file, int line);
void _debug_assertFalse (bool cond, char *message, char *file, int line);
#define debug_assertTrue(cond, message) \
	_debug_assertTrue(cond, message, __FILE__, __LINE__)
#define debug_assertFalse(cond, message)  \
	_debug_assertFalse(cond, message, __FILE__, __LINE__)
#define debug_assertEquals(expected, actual, message) \
	do { \
		if (actual != expected) { \
			fprintf (stderr, "%s:%d: Expected one value but got another. I " \
				"can't easily print out the values in C: %s\n", \
					__FILE__, __LINE__, message); \
		} \
		exit (0); \
	while (0);

#elif (!debug || NO_DAEDALUS_CHECKS)
#define debug_assertTrue(cond, message);
#define debug_assertFalse(cond, message);
#define debug_assertEquals(expected, actual, message);
#endif


#if ((debug && !NO_DAEDALUS_TRACE) || DAEDALUS_TRACE)

#define debug_trace(value) \
		fprintf (stderr, "%s:%d, value = ... I can't really print out the " \
				"value since I don't know the type\n", __FILE__, __LINE__);\


#elif (!debug || NO_DAEDALUS_TRACE)
#define debug_trace(value);
#endif
