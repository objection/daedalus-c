#include "debug.h"


void _debug_assertTrue (bool cond, char *message, char *file, int line) {
	if (!cond) {
		fprintf (stderr, "%s:%d: Expected true but was false: %s",
				file, line, message);
		exit (1);
	}
}

void _debug_assertFalse (bool cond, char *message, char *file, int line) {
	if (cond) {
		fprintf (stderr, "%s:%d: Expected false but was true: %s",
				file, line, message);
		exit (1);
	}
}

