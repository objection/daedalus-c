#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include "../lib/darr/darr.h"

#define MAX(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _a : _b; })

#define MIN(a,b) \
	({ __typeof__ (a) _a = (a); \
	 __typeof__ (b) _b = (b); \
	 _a > b? _b : _a; })


DECLARE_ARR (float, float);
DECLARE_ARR (float_arr, float_arr);
DECLARE_ARR (void *, void_p);

